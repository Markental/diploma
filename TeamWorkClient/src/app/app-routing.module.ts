import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AllDoneRequestsComponent } from './all-done-requests/all-done-requests.component';
import { ApprovedRequestsComponent } from './approved-requests/approved-requests.component';
import { CreateRequestComponent } from './create-request/create-request.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { DeatilsRequestComponent } from './deatils-request/deatils-request.component';
import { DetailsUserComponent } from './details-user/details-user.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ProfileComponent } from './profile/profile.component';
import { RequestsToApproveComponent } from './requests-to-approve/requests-to-approve.component';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: 'requests', component: HomeComponent, canActivate: [AuthGuardService]},
  { path: 'login', component: LoginComponent },
  { path: 'requests/create', component: CreateRequestComponent, canActivate: [AuthGuardService]},
  { path: 'requests/details/:id', component: DeatilsRequestComponent, canActivate: [AuthGuardService]},
  { path: '', component: MainPageComponent, canActivate: [AuthGuardService]},
  { path: 'requests/approved', component: ApprovedRequestsComponent, canActivate: [AuthGuardService]},
  { path: 'requests/pending', component: RequestsToApproveComponent, canActivate: [AuthGuardService]},
  { path: 'requests/complete', component: AllDoneRequestsComponent, canActivate: [AuthGuardService]},
  { path: 'profile', component: ProfileComponent, canActivate:[AuthGuardService]},
  { path: 'admin', component: AdminPanelComponent, canActivate: [AuthGuardService]},
  { path: 'admin/createuser', component: CreateUserComponent, canActivate:[AuthGuardService]},
  { path: 'admin/userdetails/:id', component: DetailsUserComponent, canActivate:[AuthGuardService]},

  

   
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

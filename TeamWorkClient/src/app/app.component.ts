import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TeamWorkClient';
  showLogin: boolean = false;

  constructor(){
    if(localStorage.getItem('token')===null){
       this.showLogin = true;
    }
    else
      this.showLogin = false;

  }



}




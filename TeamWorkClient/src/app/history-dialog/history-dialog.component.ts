import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RequestEventHistory } from '../models/RequestEventHistory';

@Component({
  selector: 'app-history-dialog',
  templateUrl: './history-dialog.component.html',
  styleUrls: ['./history-dialog.component.css']
})
export class HistoryDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<HistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  formatedDate(createdOn: Date){
    let date = new DatePipe('en').transform(createdOn, 'yyyy/MM/dd HH:mm:ss')?.toString();
    return date;
  }

  ngOnInit(): void {
  }
}

export interface DialogData {
  requestHistory: Array<RequestEventHistory>;
}

export interface ApplicationUser {
    Id: string;
    UserName: string;
    Email: string;
    FirstName: string;
    LastName: string;
    Position: string;
    PhoneNumber: string;
}
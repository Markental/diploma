export interface RequestStatus {
    Id: string;
    Name: string;
    Description: string;
    Flag: string;
    CreatedOn: Date;
    CreatedBy: string;
    LastUpdatedOn: Date;
    LastUpdateBy: string;
}
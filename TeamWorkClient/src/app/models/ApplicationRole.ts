export interface ApplicationRole {
    Id: string,
    Description: string,
    Name: string
}
export interface RequestEventHistory {
    Id: string;
    RequestId: string;
    Flag: string;
    CreatedOn: Date;
    CreatedBy: string;
    LastUpdatedOn: string;
    LastUpdatedBy: string;
}
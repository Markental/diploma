import { ApprovalListMember } from "./ApprovalListMember";

export interface ApprovalList{
    Id: string;
    Members: Array<ApprovalListMember>;
    IsApproved: boolean;
    CancelMessage: string;
    CreatedOn: string;
    CreatedBy: string;
    LastUpdatedOn: string;
    LastUpdatedBy: string;
}
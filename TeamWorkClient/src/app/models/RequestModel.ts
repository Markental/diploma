import { ApplicationUser } from "./ApplicationUser";
import { ApprovalList } from "./ApprovalList";
import { RequestEventHistory } from "./RequestEventHistory";
import { RequestFile } from "./RequestFile";
import { RequestStatus } from "./RequestStatus";

export interface RequestModel {
    Id: string;
    Name: string;
    Description: string;
    Sum: number;
    RequestStatusId: number;
    RequestStatus: RequestStatus;
    CreatorId: string;
    Creator: ApplicationUser;
    Date: Date;
    RequestFiles: Array<RequestFile>
    RequestEventHistories: RequestEventHistory;
    ApprovalList: ApprovalList;
    CreatedOn: Date;
    CreatedBy: string;
    LastUpdatedOn: Date;
    LastUpdatedBy: string;
}
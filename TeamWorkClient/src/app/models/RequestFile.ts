import { SafeResourceUrl } from "@angular/platform-browser";
import { RequestModel } from "./RequestModel";

export interface RequestFile {
    Id: string;
    Name: string;
    SafeUrl: SafeResourceUrl;
    RequestId: string;
    Length: number;
    MimeType: string;
    Extension: string;
    CreatedOn: Date;
    CreatedBy: string;
    LastUpdatedOn: Date;
    LastUpdatedBy: string;
}
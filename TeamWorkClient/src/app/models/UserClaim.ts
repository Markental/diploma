export interface UserClaim {
    Type: string;
    Value: string;
    UserId: string;
}
import { ApplicationRole } from "./ApplicationRole";
import { ApplicationUser } from "./ApplicationUser";
import { UserClaim } from "./UserClaim";

export interface UserModel {
    Item1: ApplicationUser,
    Item2: string[],
    Item3: UserClaim[]
}
import { ApplicationUser } from "./ApplicationUser";
import { ApprovalList } from "./ApprovalList";

export interface ApprovalListMember {
    Id: string;
    MemberId: string;
    Member: ApplicationUser;
    Flag: string;
    ApprovalListId: string;
    ApprovalList: ApprovalList;
    Approved: number;
    CancelMessage: string;
    CreatedOn: Date;
    CreatedBy: string;
    LastUpdatedOn: string;
    LastUpdatedBy: string;

}
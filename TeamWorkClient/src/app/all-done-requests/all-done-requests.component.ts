import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { environment } from '../../environments/environment.prod';
import { RequestModel } from '../models/RequestModel';
import { RequestTable } from '../models/RequestTable';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-all-done-requests',
  templateUrl: './all-done-requests.component.html',
  styleUrls: ['./all-done-requests.component.css']
})
export class AllDoneRequestsComponent implements OnInit, AfterViewInit {
  filteredList: Array<RequestModel> = [];
  displayedColumns: string[] = ['RequestStatusId','Creator', 'Name', 'Sum', 'Description', 'Actions'];
  dataSource = new MatTableDataSource<RequestModel>();
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  exampleDatabase!: ExampleHttpDatabase;
  constructor(private _httpClient: HttpClient, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator

  ngAfterViewInit(){
    this.exampleDatabase = new ExampleHttpDatabase(this._httpClient, this.authService);
    merge(this.paginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.exampleDatabase.getRepoIssues(
          this.paginator.pageIndex, this.paginator.pageSize);
      }),
      map(data => {
        // Flip flag to show that loading has finished.
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.resultsLength = data.Item2;
  
        return data.Item1;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        // Catch if the GitHub API has reached its rate limit. Return empty data.
        this.isRateLimitReached = true;
        return observableOf([]);
      })
    ).subscribe(data=>{
      this.dataSource.data = data;
    })
  }
  goToRequest(id: number){
    this.router.navigate(["requests/details", id])
  }


}

export class ExampleHttpDatabase {
  constructor(private _httpClient: HttpClient, private authService: AuthService) {}

  getRepoIssues(skip: number, top: number): Observable<RequestTable> {
    const path = environment.apiUrl + "requests/" + `getalldonerequests/?skip=${skip}&top=${top}`;
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this._httpClient.get<RequestTable>(path, {headers});
  }
}
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { from, merge, Observable,  of as observableOf } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { RequestsService } from '../services/requests.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  passwordForm!: FormGroup;
  hideOld: boolean = true;
  hideNew: boolean = true;
  showError: boolean = false;
  constructor(private fb: FormBuilder, private requestService: RequestsService, private router: Router) { 
    this.passwordForm = this.fb.group({
      'OldPassword': ['', Validators.required],
      'NewPassword': ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  updatePassword(){
    let newPassword = this.passwordForm.get('NewPassword')?.value;
    let oldPassword = this.passwordForm.get('OldPassword')?.value;
    this.requestService.updatePassword(oldPassword, newPassword).pipe(catchError((err)=>{
      this.showError = true;
      return observableOf([]);

    })).subscribe(data=>{
      this.router.navigate(['requests']);
    })
  }
}

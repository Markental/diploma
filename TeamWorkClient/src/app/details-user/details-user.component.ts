import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { element } from 'protractor';
import { UserClaim } from '../models/UserClaim';
import { UserModel } from '../models/UserModel';
import { RequestsService } from '../services/requests.service';

@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrls: ['./details-user.component.css']
})
export class DetailsUserComponent implements OnInit {
  roleForm!: FormGroup;
  permissionForm!: FormGroup;
  userId!: string;
  rolesList: Array<string> = [];
  permissionsList: Array<UserClaim> =[];
  user!: UserModel;
  constructor(private fb: FormBuilder, private requestService: RequestsService, private route: ActivatedRoute) { 
    this.roleForm = this.fb.group({
      'Role': [''],
    })
    this.permissionForm = this.fb.group({
      'Permission': [''],
    })
    this.requestService.getRoles().subscribe(data=>{
      data.forEach(element => {
        this.rolesList.push(element.Name);
      });
    })
    this.requestService.getPermissions().subscribe(data=>{
      data.forEach(element=>{
        this.permissionsList.push(element);
      })
    })
    this.route.params.subscribe(res=>{
      this.userId = res['id'];
      this.requestService.getOneUserById(this.userId).subscribe(data=>{
        this.user = data;
        console.log(this.user);
      })
    })
  }

  ngOnInit(): void {
    console.log(this.permissionsList);
  }

  addRole(){
    this.requestService.addRole(this.userId, this.roleForm.get('Role')?.value).subscribe(data=>{
      this.requestService.getOneUserById(this.userId).subscribe(data=>{
        this.user = data;
      })
    })
  }

  removeRole(role: string){
    this.requestService.removeRole(this.userId, role).subscribe(data=>{
      this.requestService.getOneUserById(this.userId).subscribe(data=>{
        this.user = data;
      })
    })
  }

  addPermission(){
    this.requestService.addPermission(this.userId, this.permissionForm.get('Permission')?.value).subscribe(data=>{
      this.requestService.getOneUserById(this.userId).subscribe(data=>{
        this.user = data;
      })
    })
  }

  removePermission(permission: string){
    this.requestService.removePermission(this.userId, permission).subscribe(data=>{
      this.requestService.getOneUserById(this.userId).subscribe(data=>{
        this.user = data;
      })
    })
  }

}

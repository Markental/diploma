import { DatePipe, DOCUMENT } from '@angular/common';
import { AfterViewChecked, AfterViewInit, Component, ElementRef, Inject, OnInit, PipeTransform, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { DeclineDialogComponent } from '../decline-dialog/decline-dialog.component';
import { ApplicationUser } from '../models/ApplicationUser';
import { ApprovalList } from '../models/ApprovalList';
import { ApprovalListMember } from '../models/ApprovalListMember';
import { RequestModel } from '../models/RequestModel';
import { RequestsService } from '../services/requests.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { environment } from '../../environments/environment.prod';
import { HistoryDialogComponent } from '../history-dialog/history-dialog.component';
import { RequestEventHistory } from '../models/RequestEventHistory';


@Component({
  selector: 'app-deatils-request',
  templateUrl: './deatils-request.component.html',
  styleUrls: ['./deatils-request.component.css']
})

export class DeatilsRequestComponent implements OnInit, PipeTransform{
  id!: string;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  durationInSeconds = 2;
  isApprovedButton: boolean = true;
  cancelMessage: string = "";
  approvalForm!: FormGroup;
  requestForm!: FormGroup;
  filesForm!: FormGroup;
  request!: RequestModel;
  approversList: Array<ApplicationUser> = [];
  approvalList!: ApprovalList;
  formData = new FormData();
  fileFormData = new FormData();
  approvalListMemmbers: Array<ApprovalListMember> = [];
  dataSource!: MatTableDataSource<ApprovalListMember>;
  displayedColumns: string[] = ['MemberName', 'Position', 'Status', 'Action'];
  displayedColumnsMinusAction: string[] = ['MemberName', 'Position', 'Status'];
  tabs: string[] = [];
  selected = new FormControl(0);

  constructor( @Inject(DOCUMENT) private document: Document, private _snackBar: MatSnackBar, public dialog: MatDialog, private route: ActivatedRoute, private requestsService: RequestsService, private sanitizer: DomSanitizer, private fb: FormBuilder, private router: Router) { 
    this.approvalForm = this.fb.group({
      'UserId': [''],
    })
    this.filesForm = this.fb.group({
      'Files': [''],
    })
    
    this.route.params.subscribe(res =>{
      this.id = res['id'];
      this.requestsService.getRequestById(this.id).subscribe(res =>{
        console.log(res);
        this.request = res;
        this.requestForm = this.fb.group({
          'Name': [this.request.Name, Validators.required],
          'Description': [this.request.Description, Validators.required],
          'Sum': [this.request.Sum, Validators.required],
          'Date': [this.request.Date, Validators.required],
        })
        this.request.RequestFiles.forEach(element=>{
          element.SafeUrl = this.transform(element.Id, element.Name);
        });
        let counter = 1;
        this.request.RequestFiles.forEach(element => {
          this.tabs.push('Attachment' + counter++);
        });
        this.isApprove();
        this.approvalList = res.ApprovalList;
        this.dataSource = new MatTableDataSource<ApprovalListMember>(this.approvalList.Members); 
        console.log(this.dataSource);
        
      })
    })
    this.requestsService.getApprovers().subscribe(res=>{
      this.approversList = res;     
    })  
   
  }


  ngOnInit(): void {
   
  }

  fileLength(length: number){
    return ((length/1024)/1024).toFixed(2);
  }
  

  addApprover(){
    this.formData = new FormData;
    this.formData.append('listId', this.approvalList.Id);
    this.formData.append('userId', this.approvalForm.get('UserId')?.value);
    this.requestsService.addMember(this.formData).subscribe(res=>{
      this.requestsService.getApprovalList(this.approvalList.Id).subscribe(list=>{
        this.approvalList = list;
        this.dataSource = new MatTableDataSource<ApprovalListMember>(this.approvalList.Members);
        let button = this.document.getElementById('sendForApprovalButton') as HTMLElement;
        button.removeAttribute("disabled");
        button.classList.remove('mat-button-disabled');
      })      
    })
  }

  editSnackBar() {
    this._snackBar.open('Request was edited!', 'Cancel', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.durationInSeconds * 1000
    });
  }

  deleteMemberFromApprovalList(memberId: string, listId: string){
     this.requestsService.removeMember(memberId, listId).subscribe(res=>{
       this.requestsService.getApprovalList(listId).subscribe(list=>{
        this.approvalList = list;
        this.dataSource = new MatTableDataSource<ApprovalListMember>(this.approvalList.Members);
        if(this.approvalList.Members.length===0){
          let button = this.document.getElementById('sendForApprovalButton') as HTMLElement;
          button.setAttribute("disabled", "disabled");
          button.classList.add('mat-button-disabled');
        }
       })      
     }) 
  }
  getCreatedOn(createdOn: Date){
    return createdOn.toString().substring(0, createdOn.toString().indexOf("T"));
  }
  transform(id: string, filename: string){
   return this.sanitizer.bypassSecurityTrustResourceUrl(environment.apiUrl + "requests/" + `GetFile/${id}/${filename}`);
 }

  onSelectedFileMultiple(event: any) {
  if (event.target.files.length > 0) {
    this.fileFormData = new FormData;
    let files = [];
    for (let i = 0; i < event.target.files.length; i++) {
      let file = event.target.files[i]
      files.push(file) 
    }
      files.forEach((file: File) => {
         this.fileFormData.append('Uploads', file, file.name);
         console.log(file);
         console.log(this.fileFormData);
      })
   }
 }

 addNewFileToRequest(){
  this.requestsService.addNewFilesToRequest(this.request.Id, this.fileFormData).subscribe(data=>{
    this.requestsService.getRequestFiles(this.request.Id).subscribe(files=>{
      files.forEach(element=>{
        element.SafeUrl = this.transform(element.Id, element.Name);
      })
      this.request.RequestFiles = files;
    })
  });
 }

  deleteFileFromRequest(filedId: string){
    this.requestsService.deleteFileFromRequest(filedId).subscribe(data=>{
      this.requestsService.getRequestFiles(this.request.Id).subscribe(files=>{
        files.forEach(element=>{
          element.SafeUrl = this.transform(element.Id, element.Name);
        })
        this.request.RequestFiles = files;
      })
    });

  }

  sendForApproval(){
    this.requestsService.sendForApproval(this.request.Id).subscribe(res=>{
      this.router.navigate(['requests']);
    })
 }

  getCurrentUserId(){
   return localStorage.getItem('userId');
 }

  ifCreator(){
    if(this.request.Creator.Id==localStorage.getItem('userId')){
      return false;
    }
    else
    return true;
  }

  approveStep(){
   this.requestsService.approveStep(this.request.Id).subscribe(res=>{
    this._snackBar.open('Request was approved!', 'Cancel', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.durationInSeconds * 1000
    });
     this.router.navigate(['requests']);
   })
  }
 completeStep(){
  this.requestsService.compelteStep(this.request.Id).subscribe(res=>{
    this._snackBar.open('Request was complited!', 'Cancel', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.durationInSeconds * 1000
    });
    this.router.navigate(['requests']);
  })
}

deleteRequest(){
  this.requestsService.deleteRequest(this.request.Id).subscribe(res=>{
    this._snackBar.open('Request was deleted!', 'Cancel', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: this.durationInSeconds * 1000
    });
    this.router.navigate(['requests']);
  })
}

//  declineStep(cancelMessage: string){
//    this.requestsService.declineStep(this.request.Id, cancelMessage).subscribe(res=>{
//     this._snackBar.open('Request was declined!', 'Cancel', {
//       horizontalPosition: this.horizontalPosition,
//       verticalPosition: this.verticalPosition,
//       duration: this.durationInSeconds * 1000
//     });
//       this.router.navigate(['requests']);
//    })
//  }

 checkPermission(permission: string){
  let f: boolean = false;
  let permissions: Array<string> = localStorage.getItem("permissions")!.split('|');
  permissions.forEach(element => {
    if(element === permission){
        f = true;
    }
  });
  return f;
}

openDialog(): void {
  const dialogRef = this.dialog.open(DeclineDialogComponent, {
    width: '250px',
    data: {requestId: this.request.Id, cancelMessage: this.cancelMessage}
  });

  dialogRef.afterClosed().subscribe(result => {
    if(result==null){
      
    }
    else{
    this.cancelMessage = result;
    this.requestsService.declineStep(this.request.Id, this.cancelMessage).subscribe(res=>{
      this._snackBar.open('Request was declined!', 'Cancel', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: this.durationInSeconds * 1000
      });
      this.router.navigate(['requests']);
      })
    }
  });
}


openHistory(): void {
  const dialogRef = this.dialog.open(HistoryDialogComponent, {
    width: '600px',
    data: {requestHistory: this.request.RequestEventHistories}
  });
}


isApprove(): boolean{
  let userid = localStorage.getItem('userId');
  let flag = true;
  this.request.ApprovalList.Members.forEach(element => {
    if(element.MemberId === userid){
      if(element.Approved != 0){
        flag = false;
      }
      else{
        flag = true;
      }
    }    
  });
  console.log(flag);
  return flag;
}

update(){
  let name = this.requestForm.get('Name')?.value;
  let description = this.requestForm.get('Description')?.value;
  let sum = this.requestForm.get('Sum')?.value;
  let date = new DatePipe('en').transform(this.requestForm.get('Date')?.value, 'yyyy/MM/dd HH:mm')?.toString();
  this.requestsService.editRequest(name, description, sum, date, this.request.Id).subscribe(data=>{
    this.router.navigate(['requests/details/' + this.request.Id]);
  })
}

}


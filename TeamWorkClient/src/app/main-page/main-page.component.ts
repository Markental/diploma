import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from, merge, Observable, observable, of as observableOf } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { RequestModel } from '../models/RequestModel';
import { RequestsService } from '../services/requests.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
panelOpenState: boolean = false;
request!: RequestModel;
pendingCount!: number;
approvedCount!: number;
isLoadingResults = true;
  constructor(private router: Router, private requestService: RequestsService) { 
    
  }

  ngOnInit(): void {

    this.isLoadingResults = true;
    this.requestService.getRecentRequest().subscribe(res=>{
      this.request = res;
      this.isLoadingResults = false;
      console.log(this.request);
    }, err=>{
      this.isLoadingResults = false;
      console.log("error");
    })
    this.requestService.getPendingCount().subscribe(res=>{
      this.pendingCount = res;
      this.isLoadingResults = false;
      console.log(this.pendingCount);
    })
    this.requestService.getApprovedCount().subscribe(res=>{
      this.approvedCount = res;
      this.isLoadingResults = false;
      console.log("approvedddd" + this.approvedCount);
    })
}

goToPending(){
  this.router.navigate(['requests/pending']);
}

goToApproved(){
  this.router.navigate(['requests/approve']);
}

goToRecent(){
  this.router.navigate(['requests/details/' + this.request.Id]);
}

getCreatedOn(){
  return this.request.CreatedOn.toString().substring(0, this.request.CreatedOn.toString().indexOf("T"));
}

getDate(){
  return this.request.Date.toString().substring(0, this.request.Date.toString().indexOf("T"));
}

getPendingCount(){
  return this.pendingCount;
}

getApprovedCount(){
  console.log("approved"+ this.approvedCount);
  return this.approvedCount;
}

createRequest(){
  this.router.navigate(['requests/create'])
}

checkPermission(permission: string){
  let f: boolean = false;
  let permissions: Array<string> = localStorage.getItem("permissions")!.split('|');
  permissions.forEach(element => {
    if(element === permission){
        f = true;
    }
  });
  return f;
}

}

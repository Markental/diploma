import { AfterContentInit, AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { filter, map, shareReplay, takeUntil } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import { MatAccordion } from '@angular/material/expansion';


@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent{
  public showLogin: boolean = false;
  currentUrl!: string;
  title!: string;
  expanded: boolean = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private location: Location, private authService: AuthService, private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd ) {
        this.currentUrl = event.url;
      }
    });
    if (window.location.href.indexOf('/requests/pending') > -1) {
      this.title = "Requests To Approve";
      this.expanded = true;
    }
    else if(window.location.href.indexOf('/requests/approved') > -1) {
      this.title = "Approved Requests";
      this.expanded = true;
    }
    else if(window.location.href.indexOf('/requests/complete') > -1) {
      this.title = "Completed Requests";
      this.expanded = true;
    }
    else if(window.location.href.indexOf('/requests/create') > -1) {
      this.title = "Create New Request";
      this.expanded = true;
    }
    else if(window.location.href.indexOf('/requests/details/') > -1) {
      this.title = "Request Details";
      this.expanded = true;
    }
    else if(window.location.href.indexOf('/requests') > -1) {
      this.title = "Your Requests";  
      this.expanded = true;
    }
    else if(window.location.href.indexOf('/admin/userdetails') > -1) {
      this.title = "User Details";
      this.expanded = false;
    }
    else if(window.location.href.indexOf('/admin/createuser') > -1) {
      this.title = "Create New User";
      this.expanded = false;
    }
    else if(window.location.href.indexOf('/admin') > -1) { // place after all '/admin/something' or other admin pages won't work
      this.title = "List Of Users";
      this.expanded = false;
    }
    else if(window.location.href.indexOf('/profile') > -1) {
      this.title = "Profile";
      this.expanded = false;
    }
    else if(window.location.href.indexOf('') > -1) {
      this.title = "Home";
      this.expanded = false;
    } 
  }


 userName(){
    return localStorage.getItem("fullName");
  }

  

  checkPermission(permission: string){
    let f: boolean = false;
    let permissions: Array<string> = localStorage.getItem("permissions")!.split('|');
    permissions.forEach(element => {
      if(element === permission){
          f = true;
      }
    });
    return f;
  }
  ifPending(){
    if ("/requests/pending"===this.currentUrl) {
      return "selected";
    }
    else
      return "";
  }

  ifRequests(){
    
    if ("/requests"===this.currentUrl) {
      return "selected";
    }
    else
      return "";
  }
  ifApproved(){
    if ("/requests/approved"===this.currentUrl) {
      return "selected";
    }
    else
      return "";
  }

  ifComplete(){
    if ("/requests/complete"===this.currentUrl) {
      return "selected";
    }
    else
      return "";
  }

  ifHome(){
    
    if ("/"===this.currentUrl) {
      return "selected";
    }
    else
      return "";
  }

  back(){
    this.location.back();
  }
  getTitle(){  
      return this.title;
    }

  logout(){
    localStorage.clear();
    window.location.reload();
  }

  isAuthenticated() {
    if (this.authService.getToken()){
      return true;
    }
    return false;
  }

}


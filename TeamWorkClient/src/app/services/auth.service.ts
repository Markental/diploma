import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment} from "../../environments/environment.prod"

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginPath = environment.apiUrl + 'identity/login'
  constructor(private http: HttpClient) { }

  login(data: any): Observable<any> {
    return this.http.post(this.loginPath, data);
  }

  saveToken(data: any) {
    localStorage.setItem('token', data.token)
  }

  getToken(){
    return localStorage.getItem('token')
  }

  isAuthenticated() {
    if (this.getToken()){
      return true;
    }
    return false;
  }

}

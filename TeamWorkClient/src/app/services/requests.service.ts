import { Injectable } from '@angular/core';
import { environment} from "../../environments/environment.prod"
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { RequestModel } from '../models/RequestModel';
import { AuthService } from './auth.service';
import { ApplicationUser } from '../models/ApplicationUser';
import { ApprovalList } from '../models/ApprovalList';
import { RequestTable } from '../models/RequestTable';
import { RequestFile } from '../models/RequestFile';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  private requestsPath = environment.apiUrl + 'requests/'
  constructor(private http: HttpClient, private authService: AuthService) { }

  getRequests(): Observable<Array<RequestModel>> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<Array<RequestModel>>(this.requestsPath + "getfilteredlist", {headers});
  }

  // getRequestsLazyLoading(skip: number, top: number): Observable<RequestTable> {
  //   let headers = new HttpHeaders();
  //   headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
  //   const path = `${this.requestsPath}getfilteredlist/?skip=${skip}&top=${top}`;
  //   return this.http.get<RequestTable>(path , {headers});
  // }



  getRequestById(id: string): Observable<RequestModel> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<RequestModel>(this.requestsPath + "GetRequestById/" + id,  {headers});
  }

  updatePassword(OldPassword: string, NewPassword: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(environment.apiUrl + "Identity/ChangePassword", {OldPassword, NewPassword}, {headers});
  }

  createRequest(data: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<RequestModel>(this.requestsPath + "CreateWhole", data, {headers});
  }

  deleteRequest(id: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.delete(this.requestsPath + "DeleteRequest/"+ id, {headers});
  }

  addRole(userId: string, role: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(environment.apiUrl + "Admin/AddRole/" + userId + `/?role=${role}`, {}, {headers});
  }

  removeRole(userId: string, role: string){
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(environment.apiUrl + "Admin/RemoveRole/" + userId + `/?role=${role}`, {}, {headers});
  }

  addPermission(userId: string, permission: string){
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(environment.apiUrl + "Admin/AddPermission/" + userId + `/?permission=${permission}`, {}, {headers});
  }

  removePermission(userId: string, permission: string){
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(environment.apiUrl + "Admin/RemovePermission/" + userId + `/?permission=${permission}`, {}, {headers});
  }

  getRoles(): Observable<any[]>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`);
    return this.http.get<any[]>(environment.apiUrl + "Admin/GetRoles",  {headers});
  }

  getPermissions(): Observable<any[]>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`);
    return this.http.get<any[]>(environment.apiUrl + "Admin/GetPermissions",  {headers});
  }

  getOneUserById(userId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`);
    return this.http.get(environment.apiUrl + "Admin/GetOneUser/" + userId,  {headers});
  }

  createUser(data: any): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(environment.apiUrl + "Admin/CreateUser", data, {headers});
  }

  editRequest(name: string, description: string, sum: number, date: any, requestId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    let url = this.requestsPath + `edit/?requestId=${requestId}&name=${name}&description=${description}&sum=${sum}&date=${date}`;
    return this.http.put(url, {}, {headers});
  }

  deleteFileFromRequest(fileId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.delete(this.requestsPath + "DeleteFileFromRequest/"+ fileId, {headers});
  }

  addNewFilesToRequest(requestId: string, data: any): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post(this.requestsPath + "AddNewFilesToRequest/" + requestId, data, {headers})
  }

  getRequestFiles(requestId: string): Observable<Array<RequestFile>>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    let url = this.requestsPath + `GetRequestFiles/${requestId}`;
    return this.http.get<Array<RequestFile>>(url, {headers});
  }

  getApprovers(): Observable<Array<ApplicationUser>>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<Array<ApplicationUser>>(environment.apiUrl + "Users/GetApprovers", {headers});
  }

  getApprovalList(id: string): Observable<ApprovalList>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<ApprovalList>(environment.apiUrl + "ApprovalList/GetByRequestId/" + id, {headers});
  }

  removeMember(MemberId: string, ListId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<any>(environment.apiUrl + "ApprovalList/RemoveMember", {MemberId, ListId}, {headers});
  }

  addMember(data: any): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<any>(environment.apiUrl + "ApprovalList/AddMember", data, {headers});
  }

  sendForApproval(requestId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<any>(this.requestsPath + "SendForApproval", {requestId}, {headers});
  }

  approveStep(requestId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<any>(environment.apiUrl + "ApprovalList/ApproveStep", {requestId}, {headers});
  }

  compelteStep(requestId: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<any>(this.requestsPath + "RequestCompleted", {requestId}, {headers});
  }

  declineStep(requestId: string, cancelMessage: string): Observable<any>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.post<any>(environment.apiUrl + "ApprovalList/DeclineStep", {requestId, cancelMessage}, {headers});
  }

  getApprovedRequests(): Observable<Array<RequestModel>> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<Array<RequestModel>>(this.requestsPath + "GetAllApprovedRequests", {headers});
  }

  getRequestsToApprove(): Observable<Array<RequestModel>> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<Array<RequestModel>>(this.requestsPath + "GetRequestsToApprove", {headers});
  }

  getRecentRequest(): Observable<RequestModel>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<RequestModel>(this.requestsPath + "GetRecent", {headers});
  }

  getPendingCount(): Observable<number>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<number>(this.requestsPath + "GetPendingCount", {headers});
  }

  getApprovedCount(): Observable<number>{
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this.http.get<number>(this.requestsPath + "GetApprovedCount", {headers});
  }

}

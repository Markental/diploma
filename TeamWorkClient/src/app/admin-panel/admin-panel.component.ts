import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { ApplicationUser } from '../models/ApplicationUser';
import { UsersTable } from '../models/UsersTable';
import { AuthService } from '../services/auth.service';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit, AfterViewInit {
  exampleDatabase!: ExampleHttpDatabase;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  displayedColumns: string[] = ['Name','UserName', 'Position', 'PhoneNumber', 'Actions'];
  dataSource = new MatTableDataSource<ApplicationUser>();
  constructor(private authService: AuthService, private _httpClient: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }
  
  @ViewChild(MatPaginator) paginator!: MatPaginator

  ngAfterViewInit(){
    this.exampleDatabase = new ExampleHttpDatabase(this._httpClient, this.authService);
    merge(this.paginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;
        return this.exampleDatabase.getRepoIssues(
          this.paginator.pageIndex, this.paginator.pageSize);
      }),
      map(data => {
        // Flip flag to show that loading has finished.
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.resultsLength = data.Item2;
  
        return data.Item1;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        // Catch if the GitHub API has reached its rate limit. Return empty data.
        this.isRateLimitReached = true;
        return observableOf([]);
      })
    ).subscribe(data=>{
      this.dataSource.data = data;
    })
  }

  createUser(){
    this.router.navigate(['admin/createuser'])
  }

  goToUser(id: string){
    this.router.navigate(["admin/userdetails", id])
  }

}
export class ExampleHttpDatabase {
  constructor(private _httpClient: HttpClient, private authService: AuthService) {}

  getRepoIssues(skip: number, top: number): Observable<UsersTable> {
    const path = environment.apiUrl + `Admin/GetUsersList/?skip=${skip}&top=${top}`;
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', `Bearer ${this.authService.getToken()}`)
    return this._httpClient.get<UsersTable>(path, {headers});
  }
}
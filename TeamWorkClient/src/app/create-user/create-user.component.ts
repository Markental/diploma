import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RequestsService } from '../services/requests.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  userForm!: FormGroup;
  hide: boolean = true;
  constructor(private formBuilder: FormBuilder, private router: Router, private requestService: RequestsService) { 
    this.userForm = this.formBuilder.group({
      UserName: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]) ,
      'Password': ['', Validators.required],
      'FirstName': ['', Validators.required],
      'LastName': ['', Validators.required],
      'Position': ['', Validators.required],
      'PhoneNumber': ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  create(){
    this.requestService.createUser(this.userForm.value).subscribe(data=>{
      this.router.navigate(['admin/userdetails/' + data.UserId]);
    })
  }

}

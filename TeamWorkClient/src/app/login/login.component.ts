import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router';
import { from, merge, Observable,  of as observableOf } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { UserClaim } from '../models/UserClaim';
import { MainNavComponent } from '../main-nav/main-nav.component';
import { catchError } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  showError: boolean = false;
  userClaim: Array<UserClaim>;
  hide: boolean = true;
  permissions: string = "";
  constructor(public ngZone: NgZone, private fb: FormBuilder, private authService: AuthService, private router: Router) { 
    this.loginForm = this.fb.group({
      'username': ['', [Validators.required]],
      'password': ['', Validators.required]
    })
    this.userClaim = Array<UserClaim>();
  }

  ngOnInit(): void {
  }

  login(){
    this.showError = false;
    this.authService.login(this.loginForm.value).pipe(catchError((err)=>{
      this.showError = true;
      return observableOf([]);

    })).subscribe(data =>{
      console.log(data);
      data.claims.forEach((element: any) => {
        let claims: UserClaim = {Type: element.Type, Value: element.Value, UserId: data.Id} 
        this.userClaim.push(claims);
      });

      localStorage.setItem("token", data.token);
      localStorage.setItem("userName", data.UserName);
      localStorage.setItem("fullName", data.FullName);
      localStorage.setItem("userId", data.Id);
      
      this.userClaim.forEach(element => {
        if(element.Type === "FirstName"){
          localStorage.setItem("firstName", element.Value);
        }
        else if(element.Type === "LastName"){
          localStorage.setItem("lastName", element.Value);
        }
        else if(element.Type === "Position"){
          localStorage.setItem("position", element.Value);
        }
        else if(element.Type === "Permissions"){
          if(this.permissions === ""){
            this.permissions = this.permissions + element.Value;
          }
          else{
            this.permissions = this.permissions + "|" + element.Value;
          }
        }
      });
      
      localStorage.setItem("permissions", this.permissions);
      this.ngZone.run(()=> {});
      this.router.navigate(['']);
      window.location.href="/"
    })
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }


}

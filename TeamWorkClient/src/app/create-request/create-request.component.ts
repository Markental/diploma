import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { RequestsService } from '../services/requests.service';
import { DatePipe } from '@angular/common';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.css']
})
export class CreateRequestComponent implements OnInit {
  requestForm!: FormGroup;
  files:any[] = [];
  formData = new FormData();
  durationInSeconds = 2;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private _snackBar: MatSnackBar, private formBuilder: FormBuilder, private authService: AuthService, private router: Router, private requestService: RequestsService) { 
    this.requestForm = this.formBuilder.group({
      'Name': ['', Validators.required],
      'Description': ['', Validators.required],
      'Sum': ['', Validators.required],
      'Date': ['', Validators.required],
      'Files': [''],

    })
  }

  ngOnInit(): void {
  }

  create(){
    this.formData.append('Name', this.requestForm.get('Name')?.value);
    this.formData.append('Description', this.requestForm.get('Description')?.value);
    this.formData.append('Sum', this.requestForm.get('Sum')?.value);
    let date = new DatePipe('en').transform(this.requestForm.get('Date')?.value, 'yyyy/MM/dd HH:mm')?.toString();
    this.formData.append('Date', date!);
    this.requestService.createRequest(this.formData).subscribe(data=>{
      this._snackBar.open('Request was created!', 'Cancel', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: this.durationInSeconds * 1000
      });
      this.router.navigate(['requests/details/' + data.RequestId]);
    })
  }

  get f(){
    return  this.requestForm.controls;
 }


public onSelectedFileMultiple(event: any) {
    if (event.target.files.length > 0) {
      this.formData = new FormData;
      this.files = [];
      for (let i = 0; i < event.target.files.length; i++) {
        console.log(this.files)
        let file = event.target.files[i]
        this.files.push(file) 
      }
      this.files.forEach((file: File) => {
           this.formData.append('Files', file, file.name);
         })
     }
   }


  get name(){
    return this.requestForm.get('Name');
  }

  get description(){
    return this.requestForm.get('Description');
  }

  get sum(){
    return this.requestForm.get('Sum');
  }

  get date(){
    return this.requestForm.get('Date');
  }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkAPI.Core.Abstractions
{
    public static class Policies
    {
        public const string CanApproveRequestPolicy = "CanApproveRequestPolicy";

        public const string CanPerformRequestPolicy = "CanPerformRequestPolicy";

        public const string CanCreateRequestPolicy = "CanCreateRequestPolicy";

        public const string CanEditUsersPolicy = "CanEditUsersPolicy";
    }
}

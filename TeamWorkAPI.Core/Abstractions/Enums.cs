﻿namespace TeamWorkAPI.Core.Abstractions
{
    public class Enums
    {
        public enum RequestStatusEnum
        {
            /// <summary>
            /// Создан
            /// </summary>
            CREATED = 1,
            /// <summary>
            /// В процессе
            /// </summary>
            PENDING = 2,
            /// <summary>
            /// Отменен
            /// </summary>
            DENIED = 3,
            /// <summary>
            /// Принят
            /// </summary>
            APPROVED = 4,
            /// <summary>
            /// Принят
            /// </summary>
            DONE = 5,
            /// <summary>
            /// Удален
            /// </summary>
            DELETED = 6
        }

        public enum RequestEventEnum
        {
            CREATED = 1, 
            EDITED = 2, 
            DELETED = 3, 
            APPROVED = 4, 
            DENIED = 5,
            DONE = 6,
            SENT_FOR_APPROVAL=7
        }

        public enum MemberTypeEnum 
        {
            APPROVER = 1,
            PERFORMER = 2
        }
    }
}

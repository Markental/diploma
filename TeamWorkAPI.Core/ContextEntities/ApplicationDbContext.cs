﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace TeamWorkAPI.Core.ContextEntities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApprovalList>()
                .HasMany(l => l.Members)
                .WithOne(m => m.ApprovalList);

            builder.Entity<ApplicationUser>()
                .HasMany(l => l.ApprovalListMembers)
                .WithOne(m => m.Member);

            //builder
            //    .Entity<Request>()
            //    .Property(r => r.State)
            //    .HasConversion<int>();
        }

        #region DBSets

        public DbSet<Request> Requests { get; set; }

        public DbSet<RequestFile> RequestFiles { get; set; }

        public DbSet<RequestStatus> RequestStatuses { get; set; }

        public DbSet<RequestEventHistory> RequestEventHistories { get; set; }

        public DbSet<ApprovalList> ApprovalLists { get; set; }

        public DbSet<ApprovalListMember> ApprovalListMembers { get; set; }

        #endregion
    }
}

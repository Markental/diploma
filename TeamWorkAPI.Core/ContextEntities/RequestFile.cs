﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamWorkAPI.Core.ContextEntities
{
    [Table("RequestFiles")]
    public class RequestFile : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(300)]
        public string Name { get; set; }

        /// <summary>
        /// Заявка
        /// </summary>
        [ForeignKey("Request")]
        public Guid RequestId { get; set; }
        public virtual Request Request { get; set; }

        /// <summary>
        /// Mongo id/Azure blob id/physical path
        /// </summary>
        public string StorageId { get; set; }

        /// <summary>
        /// Размер в байтах
        /// </summary>
        public long Length { get; set; }

        public string Path { get; set; }
        public string FileType { get; set; }

        [StringLength(100)]
        public string MimeType { get; set; }

        [StringLength(100)]
        public string Extension { get; set; }

        #region ITrackable

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public string LastUpdatedBy { get; set; }

        #endregion
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TeamWorkAPI.Core.ContextEntities
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }

        public string FullName()
            => LastName + " " + FirstName;

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        [Required]
        public string Position { get; set; }

        /// <summary>
        /// Список заявок
        /// </summary>
        public virtual IEnumerable<Request> Requests { get; set; }

        /// <summary>
        /// Список апрувал участников
        /// </summary>
        public virtual IEnumerable<ApprovalListMember> ApprovalListMembers { get; set; }
    }
}

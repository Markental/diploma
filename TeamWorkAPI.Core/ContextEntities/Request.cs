﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamWorkAPI.Core.ContextEntities
{
    [Table("Requests")]
    public class Request : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public double? Sum { get; set; }

        /// <summary>
        /// Дата исполнение заявки / дедлайн
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Статус заявки
        /// </summary>
        [ForeignKey("RequestStatus")]
        public Guid RequestStatusId { get; set; }
        public virtual RequestStatus RequestStatus { get; set; }

        /// <summary>
        /// Создатель заявки
        /// </summary>
        [ForeignKey("Creator")]
        public string CreatorId { get; set; }
        public virtual ApplicationUser Creator { get; set; }

        /// <summary>
        /// Список файлов/вложений
        /// </summary>
        public virtual IEnumerable<RequestFile> RequestFiles { get; set; }

        /// <summary>
        /// Список истории
        /// </summary>
        public virtual IEnumerable<RequestEventHistory> RequestEventHistories { get; set; }

        /// <summary>
        /// One to one relationship
        /// </summary>
        public virtual ApprovalList ApprovalList { get; set; }

        #region ITrackable

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public string LastUpdatedBy { get; set; }

        #endregion
    }
}

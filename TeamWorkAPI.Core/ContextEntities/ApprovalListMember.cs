﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.ContextEntities
{
    [Table("ApprovalListMembers")]
    public class ApprovalListMember : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Участник оборота заявки
        /// </summary>
        [ForeignKey("Member")]
        public string MemberId { get; set; }
        public ApplicationUser Member { get; set; }


        /// <summary>
        /// Уникальный флаг для фильтрации
        /// </summary>
        public MemberTypeEnum Flag { get; set; }

        /// <summary>
        /// Оборот заявки
        /// </summary>
        [ForeignKey("ApprovalList")]
        public Guid ApprovalListId { get; set; }
        public virtual ApprovalList ApprovalList { get; set; }


        /// <summary>
        /// 1 - approved, 0 - pending, -1 - declined
        /// </summary>
        public int Approved { get; set; }

        /// <summary>
        /// Сообщение, в случае отказа
        /// </summary>
        public string CancelMessage { get; set; }

        #region ITrackable

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public string LastUpdatedBy { get; set; }

        #endregion
    }
}

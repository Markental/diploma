﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeamWorkAPI.Core.ContextEntities
{
    [Table("ApprovalLists")]
    public class ApprovalList : ITrackable
    {
        [Key, ForeignKey("Request")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public virtual Request Request { get; set; }

        /// <summary>
        /// Список участников обороота заявки
        /// </summary>
        public virtual IEnumerable<ApprovalListMember> Members { get; set; }

        /// <summary>
        /// True, если все ApprovalListMember нажали Approve
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// Сообщение, в случае отказа
        /// </summary>
        public string CancelMessage { get; set; }

        #region ITrackable

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public string LastUpdatedBy { get; set; }

        #endregion
    }
}

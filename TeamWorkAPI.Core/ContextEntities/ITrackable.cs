﻿using System;

namespace TeamWorkAPI.Core.ContextEntities
{
    public interface ITrackable
    {
        DateTime CreatedOn { get; set; }
        /// <summary>
        /// Application UserId
        /// </summary>
        string CreatedBy { get; set; }
        DateTime? LastUpdatedOn { get; set; }
        /// <summary>
        /// Application UserId
        /// </summary>
        string LastUpdatedBy { get; set; }
    }
}

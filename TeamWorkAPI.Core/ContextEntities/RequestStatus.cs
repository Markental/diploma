﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.ContextEntities
{
    [Table("RequestStatuses")]
    public class RequestStatus : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Уникальный флаг для фильтрации
        /// </summary>
        public RequestStatusEnum Flag { get; set; }

        public virtual IEnumerable<Request> Requests { get; set; }

        #region ITrackable

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public string LastUpdatedBy { get; set; }

        #endregion
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.ContextEntities
{
    [Table("RequestEventHistories")]
    public class RequestEventHistory : ITrackable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// Заявка
        /// </summary>
        [ForeignKey("Request")]
        public Guid RequestId { get; set; }
        public virtual Request Request { get; set; }

        /// <summary>
        /// Уникальный флаг для фильтрации
        /// </summary>
        public RequestEventEnum Flag { get; set; }

        #region ITrackable

        public DateTime CreatedOn { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdatedOn { get; set; }

        public string LastUpdatedBy { get; set; }

        #endregion
    }
}

﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TeamWorkAPI.Core;
using TeamWorkAPI.Core.ApplicationSettingModels;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Services;
using TeamWorkAPI.Core.Services.Repositories;

namespace TeamWorkAPI.Infrastructure.Extensions
{
    public class EnumSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (context.Type.IsEnum)
            {
                model.Enum.Clear();
                Enum.GetNames(context.Type)
                    .ToList()
                    .ForEach(n => model.Enum.Add(new OpenApiString(n)));
            }
        }
    }

    public class AuthorizeFiltersControllerConvention : IControllerModelConvention
    {
        public bool IsDevelopment { get; set; }

        public AuthorizeFiltersControllerConvention(bool isDevelopment)
        {
            IsDevelopment = isDevelopment;
        }

        public void Apply(ControllerModel controller)
        {
            if (!IsDevelopment)
            {
                controller.Filters.Add(new RequireHttpsAttribute());
            }

            controller.Filters.Add(new AuthorizeFilter());
        }
    }

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomAuthorization(this IServiceCollection services)
            => services.AddAuthorization(x =>
            {
                x.AddPolicy("CanApproveRequestPolicy", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("Permissions", "CanApproveRequest");
                });
                x.AddPolicy("CanPerformRequestPolicy", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("Permissions", "CanPerformRequest");
                });
                x.AddPolicy("CanCreateRequestPolicy", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("Permissions", "CanCreateRequest");
                });
                x.AddPolicy("CanEditUsersPolicy", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("Permissions", "CanEditUsers");
                });
            });

        public static void AddCustomMvc(this IServiceCollection services, bool isDevelopment)
            => services.AddMvc(x =>
            {
                x.Conventions.Add(new AuthorizeFiltersControllerConvention(isDevelopment));
                // x.Filters.Add(typeof(ValidateModelStateAttribute));
                // x.EnableEndpointRouting = false;
            })
            // .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddNewtonsoftJson(x =>
            {
                x.SerializerSettings.ContractResolver = new DefaultContractResolver();
                x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                x.SerializerSettings.Formatting = Formatting.Indented;

                x.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });

        public static IServiceCollection AddCookieSettings(this IServiceCollection services)
            => services.Configure<CookiePolicyOptions>(options =>
                {
                    options.CheckConsentNeeded = context => true;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                });

        public static IServiceCollection AddDatabase(this IServiceCollection services,
            IConfiguration configuration)
                => services
                    .AddDbContext<ApplicationDbContext>(options => options
                        .UseSqlServer(configuration.GetDefaultConnectionString()));

        public static IServiceCollection AddIdentity(this IServiceCollection services)
        {
            services
                .AddIdentity<ApplicationUser, ApplicationRole>(options =>
                {
                    options.Password.RequireDigit = true;
                    options.Password.RequireLowercase = true;
                    options.Password.RequireNonAlphanumeric = true;
                    options.Password.RequireUppercase = true;
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 1;

                    // Lockout settings
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
                    options.Lockout.MaxFailedAccessAttempts = 5;
                    options.Lockout.AllowedForNewUsers = true;

                    // User settings
                    options.User.AllowedUserNameCharacters =
                        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                    // When set to true and if few users have same email, all tokens/codes are invalid after generation
                    options.User.RequireUniqueEmail = true;

                    // Default SignIn settings
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            return services;
        }

        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services,
            ApplicationSettings appSettings)
        {
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        var principal = context.Principal;
                        var identity = principal.Identities.FirstOrDefault();
                        if (identity != null)
                        {
                            // Add the access_token as a claim, as we may actually need it
                            if (context.SecurityToken is JwtSecurityToken accessToken)
                            {
                                var accessTokenClaim = identity.Claims.FirstOrDefault(x => x.Type.Equals("access_token"));
                                if (accessTokenClaim != null)
                                {
                                    identity.RemoveClaim(accessTokenClaim);
                                }

                                identity.AddClaim(new Claim("access_token", accessToken.RawData));
                            }
                        }

                        return Task.CompletedTask;
                    }
                };
            });

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
            => services
                .AddTransient<IIdentityService, IdentityService>()
                .AddTransient<IRequestsService, RequestsService>()
                .AddTransient<IApprovalListService, ApprovalListService>()
                .AddTransient<IRequestFilesService, RequestFilesService>()
                .AddTransient<IRequestEventsService, RequestEventsService>()
                .AddTransient<FileRepository>();

        public static ApplicationSettings GetApplicationSettings(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.GetSection("ApplicationSettings");
            services.Configure<ApplicationSettings>(applicationSettings);

            var mongoSettings = configuration.GetSection("MongoStorageSettings");
            services.Configure<MongoStorageSettings>(mongoSettings);

            return applicationSettings.Get<ApplicationSettings>();
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
            => services.AddSwaggerGen(c =>
                {
                    // Swagger 2.+ support
                    //var security = new Dictionary<string, IEnumerable<string>>
                    //{
                    //    {"Bearer", new string[] { }},
                    //};

                    //c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    //{
                    //    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    //    Name = "Authorization",
                    //    In = ParameterLocation.Header,
                    //    Type = SecuritySchemeType.ApiKey, // OAuth2
                    //    Scheme = "Bearer"
                    //});

                    //c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    //{
                    //    {
                    //        new OpenApiSecurityScheme
                    //        {
                    //            Reference = new OpenApiReference
                    //            {
                    //                Type = ReferenceType.SecurityScheme,
                    //                Id = "Bearer"
                    //            },
                    //            Scheme = "oauth2",
                    //            Name = "Bearer",
                    //            In = ParameterLocation.Header,
                    //        },
                    //        new List<string>()
                    //    }
                    //});

                    c.OrderActionsBy((apiDesc) => $"{apiDesc.ActionDescriptor.RouteValues["controller"]}_{apiDesc.HttpMethod}");

                    c.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = "TeamWorkAPI",
                        Version = "v1",
                        Description = "TeamWorkAPI",
                        TermsOfService = new Uri("https://olzhik.kz"),
                        Contact = new OpenApiContact
                        {
                            Name = "Olzhik",
                            Email = "olzhik@olzhik.kz"
                        }
                    });

                    // Set the comments path for the Swagger JSON and UI.
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    c.IncludeXmlComments(xmlPath);

                    c.SchemaFilter<EnumSchemaFilter>();
                });

        //public static void AddControllersAndFilters(this IServiceCollection services)
        //    => services
        //        .AddControllers(options => 
        //        { 
        //            options
        //                .Filters
        //                .Add<ModelOrNotFoundActionFilter>();
        //        });
    }
}

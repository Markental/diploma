﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using TeamWorkAPI.Core.Models.Identity;

namespace TeamWorkAPI.Infrastructure.Extensions
{
    public static class IdentityExtensions
    {
        public static CurrentUserModel GetCurrentUser(this ClaimsPrincipal user)
        {
            var claims = user.Claims.Where(c => c.Type == "Permissions");
            List<string> permissions = new List<string>();
            if (claims.Count() != 0)
            {
                foreach (Claim claim in claims)
                {
                    permissions.Add(claim.Value);
                }
            }

            var currentUser = new CurrentUserModel
            {
                Id = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value,
                UserName = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value,
                FirstName = user.Claims.FirstOrDefault(c => c.Type == "FirstName")?.Value,
                LastName = user.Claims.FirstOrDefault(c => c.Type == "LastName")?.Value,
                Role = user.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value,
                Permissions = permissions,
                AccessToken = user.Claims.FirstOrDefault(c => c.Type == "access_token")?.Value
            };

            return currentUser;
        }
    }

}


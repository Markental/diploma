﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TeamWorkAPI.Core.ContextEntities;

namespace TeamWorkAPI.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void ApplyMigrations(this IApplicationBuilder app)
        {
            using (var services = app.ApplicationServices.CreateScope())
            {
                var dbContext = services.ServiceProvider.GetService<ApplicationDbContext>();
                dbContext.Database.Migrate();
            }
        }

        public static IApplicationBuilder UseSwaggerUI(this IApplicationBuilder app)
            => app.UseSwagger()
                .UseSwaggerUI(options =>
                {
                    options.DocumentTitle = "TeamWorkAPI";

                    // Доп. функциональность
                    options.ConfigObject.DefaultModelRendering = Swashbuckle.AspNetCore.SwaggerUI.ModelRendering.Model;
                    options.ConfigObject.DocExpansion = Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.List;
                    options.ShowExtensions();
                    options.DisplayRequestDuration();
                    options.DisplayOperationId();
                    options.EnableFilter();

                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "TeamWorkAPI v1");
                    options.RoutePrefix = string.Empty;
                });
    }
}

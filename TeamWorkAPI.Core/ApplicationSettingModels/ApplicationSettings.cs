﻿namespace TeamWorkAPI.Core.ApplicationSettingModels
{
    public class ApplicationSettings
    {
        public string Secret { get; set; }
        public string SendGridApiKey { get; set; }
    }
}

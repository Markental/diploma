﻿namespace TeamWorkAPI.Core.ApplicationSettingModels
{
    public class MongoStorageSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}

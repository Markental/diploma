﻿using System.ComponentModel.DataAnnotations;

namespace TeamWorkAPI.Core.Models.Identity
{
    public class ChangePasswordRequestModel
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}

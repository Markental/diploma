﻿using System.ComponentModel.DataAnnotations;

namespace TeamWorkAPI.Core.Models
{
    public class RegisterRequestModel
    {
        [Required]
        public string Username { get; set; } // Email=Username
        [Required]
        public string Password { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Position { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
    }
}

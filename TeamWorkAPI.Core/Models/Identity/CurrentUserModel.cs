﻿using System.Collections.Generic;

namespace TeamWorkAPI.Core.Models.Identity
{
    public class CurrentUserModel
    {
        public string FullName()
            => LastName + " " + FirstName;

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public IEnumerable<string> Permissions { get; set; }

        public string Id { get; set; }

        public string Role { get; set; }

        public string AccessToken { get; set; }
    }
}

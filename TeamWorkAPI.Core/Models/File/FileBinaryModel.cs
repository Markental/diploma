﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace TeamWorkAPI.Core.Models.File
{
    public class FileBinaryModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonRepresentation(BsonType.String)]
        public string Name { get; set; }

        [BsonRepresentation(BsonType.String)]
        public string MimeType { get; set; }

        [BsonRepresentation(BsonType.String)]
        public string Extension { get; set; }

        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CreatedOn { get; set; }

        [BsonRepresentation(BsonType.Binary)]
        public byte[] Content { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TeamWorkAPI.Core.Models.Request
{
    public class RequestVM
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public double Sum { get; set; }
        [Required]
        public DateTime Date { get; set; }

        public IList<IFormFile> Files { get; set; }
    }
}

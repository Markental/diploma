﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkAPI.Core.Models
{
    public class RemoveMemberRequestModel
    {
        [Required]
        public Guid MemberId { get; set; }
    }
}

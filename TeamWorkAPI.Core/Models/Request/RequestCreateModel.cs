﻿using TeamWorkAPI.Core.ContextEntities;

namespace TeamWorkAPI.Core.Models
{
    public class RequestCreateModel
    {
        public ApplicationUser Offeror { get; set; }
        public string OfferorId { get; set; }
    }
}

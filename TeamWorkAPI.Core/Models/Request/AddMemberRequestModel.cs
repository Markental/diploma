﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkAPI.Core.Models
{
    public class AddMemberRequestModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public Guid ListId { get; set; }
    }
}

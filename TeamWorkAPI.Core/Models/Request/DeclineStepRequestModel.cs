﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkAPI.Core.Models.Request
{
    public class DeclineStepRequestModel
    {
        [Required]
        public Guid RequestId { get; set; }
        [Required]
        public string CancelMessage { get; set; }

    }
}

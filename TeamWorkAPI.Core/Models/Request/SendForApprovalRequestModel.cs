﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkAPI.Core.Models.Request
{
    public class SendForApprovalRequestModel
    {
        [Required]
        public Guid RequestId { get; set; }
    }
}

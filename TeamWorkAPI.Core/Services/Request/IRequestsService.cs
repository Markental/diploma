﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.Services
{
    public interface IRequestsService
    {
        Task<Request> GetOneById(Guid id);
        Task<bool> Edit(Guid requestId, string name, string description, double sum, DateTime date, CurrentUserModel user);
        Task<Guid> CreateWhole(string name, string description, double sum, DateTime date, IList<IFormFile> files, CurrentUserModel creator);
        Task<IEnumerable<RequestStatus>> GetStatuses();
        Task<bool> SendForApproval(Guid requestId, CurrentUserModel user);
        Task<(IEnumerable<Request> Collection, int TotalCount)> GetFilteredList(string userId, string name, Guid? statusId, double? sum, DateTime? dateFrom, DateTime? dateTo, int pageNumber, int pageSize);
        Task<(IEnumerable<Request> Collection, int TotalCount)> GetRequestsForCurrentUsersApproval(CurrentUserModel user, int pageNumber, int pageSize);
        Task<(IEnumerable<Request> Collection, int TotalCount)> GetApprovedRequests(int pageNumber, int pageSize);
        Task<bool> CompleteRequest(Guid requestId, CurrentUserModel user);
        Task<(IEnumerable<Request> Collection, int TotalCount)> GetCompletedRequests(int pageNumber, int pageSize);
        Task<bool> Delete(Guid id, CurrentUserModel user);
        Task<Request> RecentRequest(CurrentUserModel user);
        Task<int> PendingRequestsCount(CurrentUserModel user);
        Task<int> ApprovedRequestsCount(CurrentUserModel user);
    }
}

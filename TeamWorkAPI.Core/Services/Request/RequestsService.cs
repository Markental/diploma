﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.Services
{
    public class RequestsService : IRequestsService
    {
        private readonly ApplicationDbContext _context;
        private readonly IRequestFilesService _fileService;
        private readonly IApprovalListService _approvalListService;
        private readonly IRequestEventsService _requestEventsService;

        public RequestsService(ApplicationDbContext context, IRequestFilesService fileService, IApprovalListService approvalListService, IRequestEventsService requestEventsService)
        {
            _context = context;
            _fileService = fileService;
            _approvalListService = approvalListService;
            _requestEventsService = requestEventsService;
        }

        public async Task<Request> GetOneById(Guid id)
        {
            var request = await _context.Requests
                .Include(x => x.RequestEventHistories)
                .Include(x => x.ApprovalList)
                .ThenInclude(l => l.Members)
                .ThenInclude(m => m.Member)
                .Include(x => x.RequestFiles)
                .Include(x => x.RequestStatus)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            var deleted = _context.RequestStatuses.FirstOrDefault(x => x.Flag == RequestStatusEnum.DELETED);

            if (request != null)
            {
                if (request.RequestStatusId == deleted.Id) 
                {
                    return null;
                }

                foreach (ApprovalListMember member in request.ApprovalList.Members)
                {
                    member.Member.PasswordHash = null;
                }
            }

            if (request == null)
            {
                return null;
            }

            var creator = await _context.Users
                .Select(x => new ApplicationUser
                {
                    FirstName = x.FirstName,
                    Id = x.Id,
                    LastName = x.LastName,
                    UserName = x.UserName,
                    Position = x.Position
                }).Where(x => x.Id == request.CreatorId)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            request.Creator = creator;

            return request;
        }

        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetRequestsForCurrentUsersApproval(
            CurrentUserModel user,
            int pageNumber,
            int pageSize)
        {
            var pendingStatus = await _context.RequestStatuses
                .FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.PENDING);


            //var request = await _context.ApprovalListMembers
            //    .Include(x => x.ApprovalList)
            //        .ThenInclude(x => x.Request)
            //            .ThenInclude(x => x.RequestStatus)
            //    .Include(x => x.ApprovalList)
            //        .ThenInclude(x => x.Request)
            //            .ThenInclude(x => x.Creator)
            //    .Where(x => x.MemberId == user.Id
            //        && x.Flag == MemberTypeEnum.APPROVER
            //        && x.ApprovalList.Request.RequestStatusId == pendingStatus.Id)
            //    .Select(x => x.ApprovalList.Request)
            //    .OrderByDescending(x => x.Date)
            //    .AsNoTracking()
            //    .ToListAsync();

            //var result = request.Select(x => new // RequestVM
            //{
            //    Name = x.Name,
            //    Description = x.Description,
            //    Date = x.Date,
            //    Status = x.RequestStatus,
            //    Creator = new // CreatorVM
            //    {
            //        Id = x.Creator.Id,
            //        FirstName = x.Creator.FirstName,
            //        LastName = x.Creator.LastName,
            //        Position = x.Creator.Position,
            //        UserName = x.Creator.UserName
            //    },
            //    ApprovalList = x.ApprovalList
            //})
            //.ToList();

            var request = _context.Requests
                .Where(x => x.ApprovalList
                    .Members.Any(y => y.MemberId == user.Id
                        && y.Flag == MemberTypeEnum.APPROVER
                        && y.ApprovalList.Request.RequestStatusId == pendingStatus.Id));

            request = request
                .OrderByDescending(x => x.Date);

            var totalCount = request.Count();

            if (!(pageNumber == -1))
            {
                request = request.Skip(pageNumber * pageSize);
            }

            if (!(pageSize == -1))
            {
                request = request.Take(pageSize);
            }

            var result = await request
                .Include(x => x.Creator)
                .Include(x => x.RequestStatus)
                .AsNoTracking()
                .ToListAsync();

            foreach (Request r in result)
            {
                r.Creator.PasswordHash = null;
            }

            return (result, totalCount);
        }

        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetCompletedRequests(
            int pageNumber,
            int pageSize)
        {
            IQueryable<Request> request = _context.Requests;

            var done = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DONE);

            request = request
                .Where(x => x.RequestStatusId == done.Id)
                // TODO: можно передавать сверху в виде параметра
                .OrderByDescending(x => x.Date);

            var totalCount = request.Count();

            if (!(pageNumber == -1))
            {
                request = request.Skip(pageNumber * pageSize);
            }

            if (!(pageSize == -1))
            {
                request = request.Take(pageSize);
            }

            var result = await request
                .Include(x => x.Creator)
                .Include(x => x.RequestStatus)
                .AsNoTracking()
                .ToListAsync();

            foreach (Request r in result)
            {
                r.Creator.PasswordHash = null;
            }

            return (result, totalCount);
        }

        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetApprovedRequests(
            int pageNumber,
            int pageSize)
        {
            IQueryable<Request> request = _context.Requests;

            var approved = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.APPROVED);

            request = request
                .Where(x => x.RequestStatusId == approved.Id)
                // TODO: можно передавать сверху в виде параметра
                .OrderByDescending(x => x.Date);

            var totalCount = request.Count();

            if (!(pageNumber == -1))
            {
                request = request.Skip(pageNumber * pageSize);
            }

            if (!(pageSize == -1))
            {
                request = request.Take(pageSize);
            }

            var result = await request
                .Include(x => x.Creator)
                .Include(x => x.RequestStatus)
                .AsNoTracking()
                .ToListAsync();

            foreach (Request r in result)
            {
                r.Creator.PasswordHash = null;
            }

            return (result, totalCount);
        }

        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetFilteredList(string userId,
            string name,
            Guid? statusId,
            double? sum,
            DateTime? dateFrom,
            DateTime? dateTo,
            int pageNumber,
            int pageSize)
        {

            var deletedStatus = await _context.RequestStatuses
                .FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DELETED);

            IQueryable<Request> request = _context.Requests
                .Where(x => x.CreatorId == userId && x.RequestStatusId != deletedStatus.Id)
                // TODO: можно передавать сверху в виде параметра
                .OrderByDescending(x => x.Date);

            // Например, показывать только те, которые назначены на исполнение
            //request = request
            //    .Where(x => x.ApprovalList
            //        .Members.Any(y => y.MemberId == userId && y.Flag == MemberTypeEnum.PERFORMER));

            // Более производительный запрос
            //request = _context.ApprovalListMembers
            //    .Where(x => x.MemberId == userId && x.Flag == MemberTypeEnum.PERFORMER)
            //    .Select(x => x.ApprovalList.Request);

            if (!string.IsNullOrWhiteSpace(name))
            {
                string lowerName = name.ToLower();
                request = request
                    .Where(x => x.Name.ToLower().Contains(lowerName));
            }

            //if (!string.IsNullOrWhiteSpace(authorName))
            //{
            //    request = request
            //        .Where(x => x.Creator.FirstName.Contains(name, StringComparison.InvariantCultureIgnoreCase)
            //            || x.Creator.LastName.Contains(name, StringComparison.InvariantCultureIgnoreCase));
            //}

            if (statusId != null)
            {
                request = request
                   .Where(x => x.RequestStatusId == statusId);
            }

            if (sum != null)
            {
                request = request.Where(x => x.Sum == sum);
            }

            if (dateFrom != null)
            {
                request = request.Where(x => x.Date >= dateFrom);
            }

            if (dateTo != null)
            {
                request = request.Where(x => x.Date <= dateTo);
            }

            var totalCount = request.Count();

            // Skip and top -> pagination

            if (!(pageNumber == -1))
            {
                request = request.Skip(pageNumber * pageSize);
            }

            if (!(pageSize == -1))
            {
                request = request.Take(pageSize);
            }

            var result = await request
                .Include(x => x.Creator)
                .Include(x=>x.RequestStatus)
                .AsNoTracking()
                .ToListAsync();

            foreach (Request r in result)
            {
                r.Creator.PasswordHash = null;
            }

            return (result, totalCount);
        }

        public async Task<IEnumerable<RequestStatus>> GetStatuses()
        {
            var statuses = await _context.RequestStatuses.ToListAsync();

            statuses.Remove(statuses.Find(x=>x.Flag == RequestStatusEnum.DELETED));

            return statuses;
        }

        public async Task<bool> Delete(Guid id, CurrentUserModel user)
        {
            var request = await _context.Requests.FindAsync(id);

            if (request == null)
            {
                return false;
            }

            if (request.CreatorId != user.Id) 
            {
                return false;
            }

            //_context.Remove(request);

            request.RequestStatusId =  _context.RequestStatuses.FirstOrDefault(x => x.Flag == RequestStatusEnum.DELETED).Id;
            request.LastUpdatedBy = user.FullName();
            request.LastUpdatedOn = DateTime.Now;
            _requestEventsService.CreateEventHistory(id, RequestEventEnum.DELETED, user);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<Guid> CreateWhole(
            string name,
            string description,
            double sum,
            DateTime date,
            IList<IFormFile> files,
            CurrentUserModel creator)
        {
            // Create request
            var request = new Request
            {
                Date = date,
                Description = description,
                Name = name,
                Sum = sum,
                RequestStatusId = _context.RequestStatuses.FirstOrDefault(x => x.Flag == RequestStatusEnum.CREATED).Id,
                CreatorId = creator.Id,
                CreatedOn = DateTime.Now,
                CreatedBy = creator.FullName()
            };

            _context.Requests.Add(request);

            // Create empty approval list
            _approvalListService.Create(request.Id, creator.FullName());

            _requestEventsService.CreateEventHistory(request.Id, RequestEventEnum.CREATED, creator);

            await _context.SaveChangesAsync();

            // Attach files
            if (files != null)
            {
                await _fileService.AddFilesToMongo(files, request.Id, creator.FullName());
            }

            return request.Id;
        }

        public async Task<bool> Edit(Guid requestId, string name, string description, double sum, DateTime date, CurrentUserModel user)
        {
            var request = await _context.Requests
                .Include(x => x.RequestFiles)
                .FirstOrDefaultAsync(x => x.Id == requestId);

            var created = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.CREATED);

            bool changed = false;

            if (request == null)
            {
                return changed;
            }

            if (user.Id != request.CreatorId || request.RequestStatusId != created.Id) 
            {
                return changed;
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                request.Name = name;
                changed = true;
            }

            if (!string.IsNullOrWhiteSpace(description))
            {
                request.Description = description;
                changed = true;
            }

            if (sum != null)
            {
                request.Sum = sum;
                changed = true;
            }

            if (date != null)
            {
                request.Date = date;
                changed = true;
            }

            if (changed)
            {
                request.LastUpdatedBy = user.FullName();
                request.LastUpdatedOn = DateTime.Now;

                _requestEventsService.CreateEventHistory(requestId, RequestEventEnum.EDITED, user);

                await _context.SaveChangesAsync();
            }

            return changed;
        }

        public async Task<bool> SendForApproval(Guid requestId, CurrentUserModel user)
        {
            var request = await _context.Requests
                .Include(x => x.ApprovalList)
                .ThenInclude(l => l.Members)
                .ThenInclude(m => m.Member)
                .Include(x => x.RequestStatus)
                .FirstOrDefaultAsync(x => x.Id == requestId);

            if (request.CreatorId != user.Id)
            {
                return false;
            }

            var pending = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.PENDING);
            var created = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.CREATED);

            if (request.RequestStatusId == created.Id
                && request.ApprovalList.Members.Count() > 0)
            {
                request.RequestStatusId = pending.Id;
                request.RequestStatus = pending;
                request.LastUpdatedBy = user.FullName();
                request.LastUpdatedOn = DateTime.Now;

                _requestEventsService.CreateEventHistory(requestId, RequestEventEnum.SENT_FOR_APPROVAL, user);

                await _context.SaveChangesAsync();
                return true;
            }

            return false;
        }

        public async Task<bool> CompleteRequest(Guid requestId, CurrentUserModel user) 
        {
            var request = await _context.Requests
                .Include(x => x.ApprovalList)
                .ThenInclude(l => l.Members)
                .ThenInclude(m => m.Member)
                .Include(x => x.RequestStatus)
                .FirstOrDefaultAsync(x => x.Id == requestId);

            var done = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DONE);
            var approved = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.APPROVED);

            if (request.RequestStatusId == approved.Id)
            {
                request.RequestStatusId = done.Id;
                request.RequestStatus = done;
                request.LastUpdatedBy = user.FullName();
                request.LastUpdatedOn = DateTime.Now;

                _requestEventsService.CreateEventHistory(requestId, RequestEventEnum.DONE, user);

                await _context.SaveChangesAsync();
                return true;
            }

            return false;

        }

        public async Task<Request> RecentRequest(CurrentUserModel user) 
        {
            var deleted = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DELETED);
            return await _context.Requests
                .Include(x => x.RequestStatus)
                .Include(x => x.RequestFiles)
                .Where(x => x.CreatorId == user.Id && x.RequestStatusId != deleted.Id)
                .OrderByDescending(x => x.CreatedOn)
                .FirstOrDefaultAsync();
        }

        public async Task<int> PendingRequestsCount(CurrentUserModel user)
        {
            var pending = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.PENDING);

            var count =  _context.ApprovalListMembers
                .Where(x => x.MemberId == user.Id && x.ApprovalList.Request.RequestStatusId == pending.Id)
                .Count();

            return count;
        }

        public async Task<int> ApprovedRequestsCount(CurrentUserModel user)
        {
            var approved = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.APPROVED);
            return  _context.Requests
                .Where(x => x.RequestStatusId == approved.Id)
                .Count();
        }
    }
}

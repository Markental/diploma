﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.Services
{
    public class RequestEventsService : IRequestEventsService
    {
        private readonly ApplicationDbContext _context;

        public RequestEventsService(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Doesn't save changes to context. CALL BEFORE SaveChangesAsync()
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="action"></param>
        /// <param name="user"></param>
        public void CreateEventHistory(Guid requestId, RequestEventEnum action, CurrentUserModel user)
        {
            _context.RequestEventHistories.Add(new RequestEventHistory
            {
                RequestId = requestId,
                Flag = action,
                CreatedBy = user.FullName(),
                CreatedOn = DateTime.Now
            });
        }

        public async Task<IEnumerable<RequestEventHistory>> GetRequestEvents(Guid requestId)
        {
            var requestEvents = await _context.RequestEventHistories.Where(x => x.RequestId == requestId).ToListAsync();

            return requestEvents;
        }
    }
}

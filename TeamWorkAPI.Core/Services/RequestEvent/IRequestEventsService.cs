﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkAPI.Core.Abstractions;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;

namespace TeamWorkAPI.Core.Services
{
    public interface IRequestEventsService
    {
        void CreateEventHistory(Guid requestId, Enums.RequestEventEnum action, CurrentUserModel user);
        Task<IEnumerable<RequestEventHistory>> GetRequestEvents(Guid requestId);
    }
}

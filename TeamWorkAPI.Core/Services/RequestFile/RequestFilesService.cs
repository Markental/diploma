﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Services.Repositories;

namespace TeamWorkAPI.Core.Services
{
    public class RequestFilesService : IRequestFilesService
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly FileRepository _fileRepository;


        public RequestFilesService(ApplicationDbContext context,
            IWebHostEnvironment appEnvironment,
            FileRepository fileRepository)
        {
            _context = context;
            _appEnvironment = appEnvironment;
            _fileRepository = fileRepository;
        }

        public async Task AddFilesToMongo(IList<IFormFile> uploads, Guid requestId, string createdBy)
        {
            foreach (var upload in uploads)
            {
                using (var ms = new MemoryStream())
                {
                    upload.CopyTo(ms);
                    var fileBytes = ms.ToArray();


                    var createResult = await _fileRepository.CreateAsync(new Models.File.FileBinaryModel
                    {
                        Name = upload.FileName,
                        Extension = Path.GetExtension(upload.FileName),
                        MimeType = upload.ContentType,
                        CreatedOn = DateTime.Now,
                        Content = fileBytes
                    });

                    //var storageId = await _fileRepository.GridFsUploadAsync(upload.FileName, fileBytes);

                    var file = new RequestFile
                    {
                        Name = upload.FileName,
                        Extension = Path.GetExtension(upload.FileName),
                        CreatedOn = DateTime.Now,
                        RequestId = requestId,
                        CreatedBy = createdBy,
                        Length = upload.Length,
                        MimeType = upload.ContentType,
                        StorageId = createResult.Id.ToString()
                    };

                    _context.RequestFiles.Add(file);
                }
            }
            _context.SaveChanges();
        }

        public async Task<(RequestFile Metadata, byte[] Content)> GetMongoRequestFile(Guid id)
        {
            var requestFile = await _context.RequestFiles.FindAsync(id);

            var fileContent = await _fileRepository.GetAsync(requestFile.StorageId);

            //var fileContent = await _fileRepository.GridFsGetAsync(requestFile.StorageId);

            return (requestFile, fileContent.Content);
        }

        public async Task MongoDeleteFile(Guid id)
        {
            var requestFile = await _context.RequestFiles.FindAsync(id);

            await _fileRepository.DeleteAsync(requestFile.StorageId);

            //await _fileRepository.GridFsDeleteAsync(requestFile.StorageId);

            _context.RequestFiles.Remove(requestFile);
            await _context.SaveChangesAsync();
        }

        public async Task MongoUpdateFileContent(Guid id, byte[] newContent) 
        {
            var requestFile = await _context.RequestFiles.FindAsync(id);

            await _fileRepository.UpdateContent(requestFile.StorageId, newContent);
            
            // await _fileRepository.GridFsDeleteAsync(requestFile.StorageId);

            //var newStorageId = await _fileRepository.GridFsUploadAsync(requestFile.Name, newContent);

            //requestFile.StorageId = newStorageId.ToString();
            requestFile.Length = newContent.Length;

            await _context.SaveChangesAsync();
        }

        public async Task<RequestFile> GetRequestFile(Guid id)
        {
            var requestFile = await _context.RequestFiles
                .Include(x => x.Request)
                .FirstOrDefaultAsync(x => x.Id == id);

            return requestFile;
        }

        public async Task<IEnumerable<RequestFile>> GetRequestFiles(Guid requestId)
        {
            var requestFiles = await _context.RequestFiles
                .Where(x => x.RequestId == requestId)
                .AsNoTracking()
                .ToListAsync();

            return requestFiles;
        }
    }
}

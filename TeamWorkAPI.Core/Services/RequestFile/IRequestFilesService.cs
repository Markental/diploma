﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;

namespace TeamWorkAPI.Core.Services
{
    public interface IRequestFilesService
    {
        //Task AddFiles(IList<IFormFile> uploads, Guid requestId, string createdBy);
        Task AddFilesToMongo(IList<IFormFile> uploads, Guid requestId, string createdBy);
        Task<(RequestFile Metadata, byte[] Content)> GetMongoRequestFile(Guid id);
        Task<IEnumerable<RequestFile>> GetRequestFiles(Guid requestId);
        Task MongoDeleteFile(Guid id);
        //string GetPhysicalFilePath(string path);
        Task<RequestFile> GetRequestFile(Guid id);
        Task MongoUpdateFileContent(Guid id, byte[] newContent);
    }
}

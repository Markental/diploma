﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;

namespace TeamWorkAPI.Core.Services
{
    public interface IApprovalListService
    {
        Task<ApprovalList> GetOneByListId(Guid requestId);

        Guid Create(Guid requestId, string createdBy);

        Task<Guid> AddApproverToList(ApplicationUser user, Guid listId, string createdBy);
        Task RemoveApproverFromList(Guid memberId);
        Task<bool> ApproveStep(Guid memberId, CurrentUserModel user);
        Task<bool> DeclineStep(Guid memberId, string cancelMessage, CurrentUserModel user);
    }
}

﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;
using static TeamWorkAPI.Core.Abstractions.Enums;

namespace TeamWorkAPI.Core.Services
{
    class ApprovalListService : IApprovalListService
    {
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly ApplicationDbContext _context;
        private readonly IRequestEventsService _requestEventsService;
        private readonly IRequestFilesService _requestFilesService;
        public ApprovalListService(ApplicationDbContext context, IRequestEventsService requestEventsService, IRequestFilesService requestFilesService, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _requestEventsService = requestEventsService;
            _requestFilesService = requestFilesService;
            _appEnvironment = webHostEnvironment;
        }

        /// <summary>
        /// DOESN'T SAVE CHANGES
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="createdBy"></param>
        /// <returns></returns>
        public Guid Create(Guid requestId, string createdBy)
        {
            var approvalList = new ApprovalList
            {
                Id = requestId, // ? поставить в ApprovalList.Id -> DatabaseGeneratedOption.None ?
                IsApproved = false,
                CreatedOn = DateTime.Now,
                CreatedBy = createdBy
            };

            _context.ApprovalLists.Add(approvalList);

            return approvalList.Id;
        }

        public async Task<ApprovalList> GetOneByListId(Guid requestId)
        {
            // requestId = approvalListId
            var approvalList = await _context.ApprovalLists
                .Include(x=>x.Request) //
                .Include(x => x.Members)
                .ThenInclude(x => x.Member)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == requestId);

            foreach (ApprovalListMember user in approvalList.Members)
            {
                user.Member.PasswordHash = null;
            }

            return approvalList;
        }

        public async Task<Guid> AddApproverToList(ApplicationUser user, Guid listId, string createdBy)
        {
            var approvalList = await GetOneByListId(listId);

            var member = new ApprovalListMember
            {
                ApprovalListId = approvalList.Id,
                Approved = 0,
                CreatedBy = createdBy,
                CreatedOn = DateTime.Now,
                Flag = MemberTypeEnum.APPROVER,
                MemberId = user.Id
            };

            _context.ApprovalListMembers.Add(member);

            await _context.SaveChangesAsync();

            return member.Id;
        }

        public async Task RemoveApproverFromList(Guid memberId)
        {
            var member = await _context.ApprovalListMembers.FirstOrDefaultAsync(x => x.Id == memberId);

            _context.ApprovalListMembers.Remove(member);

            await _context.SaveChangesAsync();

        }

        public async Task<bool> ApproveStep(Guid requestId, CurrentUserModel user)
        {
            var members = await _context.ApprovalListMembers
                .Include(x => x.Member)
                .Include(x=>x.ApprovalList)
                .ThenInclude(x=>x.Request)
                .ThenInclude(x=>x.RequestStatus)
                .ThenInclude(x=>x.Requests)
                .ThenInclude(x=>x.RequestFiles)
                .Where(x => x.ApprovalList.Request.Id == requestId)
                .ToListAsync();

            bool success = false;

            var pending = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.PENDING);
            var approved = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.APPROVED);

            foreach (ApprovalListMember member in members)
            {
                if (member.ApprovalList.Request.RequestStatusId != pending.Id) 
                {
                    return false;
                }

                if (member.MemberId != user.Id)
                {
                    continue;
                }

                member.Approved = 1;
                member.LastUpdatedBy = user.FullName();
                member.LastUpdatedOn = DateTime.Now;

                _requestEventsService.CreateEventHistory(requestId, RequestEventEnum.APPROVED, user);
                
                // если нет isApproved=false на всем обороте И если статус заявки = PENDING => тогда поменять статус
                if (!member.ApprovalList.Members.Any(x => x.Approved == 0))
                {
                    member.ApprovalList.Request.RequestStatusId = approved.Id;
                    member.ApprovalList.LastUpdatedBy = user.FullName();
                    member.ApprovalList.LastUpdatedOn = DateTime.Now;
                    member.ApprovalList.Request.LastUpdatedBy = user.FullName();
                    member.ApprovalList.Request.LastUpdatedOn = DateTime.Now;

                    
                    foreach (RequestFile file in member.ApprovalList.Request.RequestFiles) // в каждый файл
                    {
                        foreach (ApprovalListMember listMember in member.ApprovalList.Members) // добавить страницу с QR кодом каждого аппрувера
                        {
                            var requestFile = await _requestFilesService.GetMongoRequestFile(file.Id);
                            Bitmap icon = new Bitmap(Path.Combine(_appEnvironment.WebRootPath, "Files", "pog.jpg")); // just a joke

                            using (MemoryStream streamQR = new MemoryStream())
                            {
                                // QR
                                QRCodeGenerator generator = new QRCodeGenerator();
                                QRCodeData qrData = generator.CreateQrCode($"Approved by {listMember.Member.FullName()} on {DateTime.Now} using ©TeamWork", QRCodeGenerator.ECCLevel.Q);
                                QRCode QR = new QRCode(qrData);

                                Bitmap bmp = QR.GetGraphic(4, Color.Black, Color.White, icon, 15, 6, true); // get graphical representation of generated QRcode
                                bmp.Save(streamQR, ImageFormat.Bmp); // write bitmap with QRcode with specific format to stream
                                // -QR

                                PdfDocument doc;
                                using (MemoryStream pdfStream = new MemoryStream(requestFile.Content))
                                {
                                    doc = PdfReader.Open(pdfStream, PdfDocumentOpenMode.Modify); // read pdf raw data from stream
                                }

                                XImage image = XImage.FromStream(streamQR); // get QR png
                                PdfPage page = doc.InsertPage(doc.PageCount); // append new blank page to the end of pdf file
                                page.Width = bmp.Width;
                                page.Height = bmp.Height;
                                page.TrimMargins.All = 0;

                                XGraphics xg = XGraphics.FromPdfPage(page); // make XGraphics object draw on new page
                                xg.DrawImage(image, 0, 0, image.PixelWidth, image.PixelHeight); // perform drawing

                                string canSave = "";
                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    if (doc.CanSave(ref canSave))
                                    {
                                        doc.Save(memoryStream, false);
                                        await _requestFilesService.MongoUpdateFileContent(requestFile.Metadata.Id, memoryStream.ToArray());
                                    }
                                }
                            }
                        }
                    }
                }
                await _context.SaveChangesAsync();
                success = true;
                return success;
            }

            return success;
        }

        public async Task<bool> DeclineStep(Guid requestId, string cancelMessage, CurrentUserModel user)
        {
            var members = await _context.ApprovalListMembers
                .Include(x => x.Member)
                .Include(x => x.ApprovalList)
                .ThenInclude(x => x.Request)
                .Where(x => x.ApprovalList.Request.Id == requestId)
                .ToListAsync();

            bool declined = false;

            var created = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.CREATED);
            var denied = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DENIED);
            var deleted = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DELETED);
            var done = await _context.RequestStatuses.FirstOrDefaultAsync(x => x.Flag == RequestStatusEnum.DONE);

            foreach (ApprovalListMember member in members)
            {
                if (member.ApprovalList.Request.RequestStatusId == created.Id 
                    || member.ApprovalList.Request.RequestStatusId == denied.Id
                    || member.ApprovalList.Request.RequestStatusId == deleted.Id
                    || member.ApprovalList.Request.RequestStatusId == done.Id) 
                {
                    return false;
                }

                if (!user.Permissions.Contains("CanPerformRequest")) {
                    if (member.MemberId != user.Id)
                    {
                        continue;
                    }
                }

                if (!user.Permissions.Contains("CanPerformRequest"))
                {
                    if (!string.IsNullOrWhiteSpace(cancelMessage))
                    {
                        member.Approved = -1;
                        member.CancelMessage = cancelMessage;
                        member.ApprovalList.CancelMessage = cancelMessage;
                        declined = true;
                        member.LastUpdatedBy = user.FullName();
                        member.LastUpdatedOn = DateTime.Now;
                        _requestEventsService.CreateEventHistory(requestId, RequestEventEnum.DENIED, user);
                    }
                }
                else 
                {
                    if (!string.IsNullOrWhiteSpace(cancelMessage))
                    {
                        // как-то показать, что отказал не Мембер, а перформер (мб просто в RequestEventHistory показать это)
                        member.ApprovalList.CancelMessage = cancelMessage;
                        declined = true;
                        member.LastUpdatedBy = user.FullName();
                        member.LastUpdatedOn = DateTime.Now;
                        _requestEventsService.CreateEventHistory(requestId, RequestEventEnum.DENIED, user);
                    }
                }
                // если хоть кто-то отказал - поменять статус на DENIED
                if (member.ApprovalList.Request.RequestStatusId == _context.RequestStatuses.FirstOrDefault(x => x.Flag == RequestStatusEnum.PENDING).Id
                    || member.ApprovalList.Request.RequestStatusId == _context.RequestStatuses.FirstOrDefault(x => x.Flag == RequestStatusEnum.APPROVED).Id)
                {
                    member.ApprovalList.Request.RequestStatusId = _context.RequestStatuses.FirstOrDefault(x => x.Flag == RequestStatusEnum.DENIED).Id;
                    member.ApprovalList.LastUpdatedBy = user.FullName();
                    member.ApprovalList.LastUpdatedOn = DateTime.Now;
                    member.ApprovalList.Request.LastUpdatedBy = user.FullName();
                    member.ApprovalList.Request.LastUpdatedOn = DateTime.Now;
                }

                await _context.SaveChangesAsync();

            }
            return declined;
        }

    }
}

﻿using System.Collections.Generic;
using System.Security.Claims;

namespace TeamWorkAPI.Core.Services
{
    public interface IIdentityService
    {
        string GenerateJwtToken(string userId, string userName, string role, string secret, IEnumerable<Claim> customClaims);
    }
}

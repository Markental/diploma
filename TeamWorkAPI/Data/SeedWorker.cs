﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Services;

namespace TeamWorkAPI.Data
{
    public class SeedWorker : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;

        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private ApplicationDbContext _context;
        private IApprovalListService _listService;

        public SeedWorker(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();

            _context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            await _context.Database.EnsureCreatedAsync(cancellationToken);

            _userManager = scope.ServiceProvider
                .GetRequiredService<UserManager<ApplicationUser>>();

            _roleManager = scope.ServiceProvider
                .GetRequiredService<RoleManager<ApplicationRole>>();

            _listService = scope.ServiceProvider
                .GetRequiredService<IApprovalListService>();

            await CreateStatuses();
            await CreateRoles();
            await CreateUsers();
            // await CreateTestRequests();
        }

        private async Task CreateStatuses()
        {
            var hasStatuses = _context.RequestStatuses.Any();
            if (!hasStatuses)
            {
                _context.RequestStatuses.Add(new RequestStatus
                {
                    Name = "Created",
                    Flag = Core.Abstractions.Enums.RequestStatusEnum.CREATED,
                    CreatedBy = "seed",
                    CreatedOn = DateTime.Now
                });

                _context.RequestStatuses.Add(new RequestStatus
                {
                    Name = "Approved",
                    Flag = Core.Abstractions.Enums.RequestStatusEnum.APPROVED,
                    CreatedBy = "seed",
                    CreatedOn = DateTime.Now
                });

                _context.RequestStatuses.Add(new RequestStatus
                {
                    Name = "Denied",
                    Flag = Core.Abstractions.Enums.RequestStatusEnum.DENIED,
                    CreatedBy = "seed",
                    CreatedOn = DateTime.Now
                });

                _context.RequestStatuses.Add(new RequestStatus
                {
                    Name = "Pending",
                    Flag = Core.Abstractions.Enums.RequestStatusEnum.PENDING,
                    CreatedBy = "seed",
                    CreatedOn = DateTime.Now
                });

                _context.RequestStatuses.Add(new RequestStatus
                {
                    Name = "Complete",
                    Flag = Core.Abstractions.Enums.RequestStatusEnum.DONE,
                    CreatedBy = "seed",
                    CreatedOn = DateTime.Now
                });

                _context.RequestStatuses.Add(new RequestStatus
                {
                    Name = "Deleted",
                    Flag = Core.Abstractions.Enums.RequestStatusEnum.DELETED,
                    CreatedBy = "seed",
                    CreatedOn = DateTime.Now
                });

                await _context.SaveChangesAsync();
            }
        }

        private async Task CreateRoles()
        {
            var hasRoles = _roleManager.Roles.Any();
            if (!hasRoles)
            {
                // Claims - CanApproveRequest, CanPerformRequest, CanCreateRequest, CanEditUsers

                // Inner save changes
                await _roleManager.CreateAsync(new ApplicationRole
                {
                    Name = "Admin",
                    Description = "Admin"
                });

                string[] permissionsClaimsAdmin = { "CanApproveRequest", "CanPerformRequest", "CanCreateRequest", "CanEditUsers" };

                var adminRole = await _context.Roles.FirstOrDefaultAsync(x => x.Name == "Admin");
                foreach (var claim in permissionsClaimsAdmin)
                {
                    await _roleManager.AddClaimAsync(adminRole,
                        new System.Security.Claims.Claim("Permissions", claim));
                }

                await _roleManager.CreateAsync(new ApplicationRole
                {
                    Name = "User",
                    Description = "User"
                });
            }
        }

        private async Task CreateUsers()
        {
            var hasUsers = _userManager.Users.Any();
            if (!hasUsers)
            {
                // Inner save changes
                await _userManager.CreateAsync(new ApplicationUser
                {
                    FirstName = "Aibek",
                    LastName = "Karatayev",
                    Position = "System administrator",
                    Email = "aibek.karatayev@gmail.com",
                    UserName = "aibek.karatayev@gmail.com"
                }, "Qwerty123!");

                await _userManager.CreateAsync(new ApplicationUser
                {
                    FirstName = "Olzhas",
                    LastName = "Karatayev",
                    Position = "CEO Assistant",
                    Email = "karataevolzhas@gmail.com",
                    UserName = "karataevolzhas@gmail.com"
                }, "Qwerty123!");

                await _userManager.CreateAsync(new ApplicationUser
                {
                    FirstName = "Darkhan",
                    LastName = "Akimbek",
                    Position = "Head of Development Team",
                    Email = "darhan_akimbekov@mail.ru",
                    UserName = "darhan_akimbekov@mail.ru"
                }, "Qwerty123!");

                await _userManager.CreateAsync(new ApplicationUser
                {
                    FirstName = "Ingkar",
                    LastName = "Kuanyshbek",
                    Position = "Accountant",
                    Email = "inkarknnn@gmail.com",
                    UserName = "inkarknnn@gmail.com"
                }, "Qwerty123!");

                var adminUser = await _context.Users.FirstOrDefaultAsync(x => x.UserName == "aibek.karatayev@gmail.com");

                string[] permissionsClaimsAdmin = { "CanApproveRequest", "CanPerformRequest", "CanCreateRequest", "CanEditUsers" };
                
                foreach (var claim in permissionsClaimsAdmin) 
                {
                    await _userManager.AddClaimAsync(adminUser,
                        new System.Security.Claims.Claim("Permissions", claim));
                }

                await _userManager.AddToRolesAsync(adminUser, new string[] { "Admin" });


                var user = await _context.Users.FirstOrDefaultAsync(x => x.UserName == "karataevolzhas@gmail.com");

                string[] permissionsClaimsUser = { "CanApproveRequest", "CanPerformRequest", "CanCreateRequest" };

                foreach (var claim in permissionsClaimsUser) 
                {
                    await _userManager.AddClaimAsync(user,
                        new System.Security.Claims.Claim("Permissions", claim));
                }

                await _userManager.AddToRolesAsync(user, new string[] { "User" });

                var approver = await _context.Users.FirstOrDefaultAsync(x => x.UserName == "darhan_akimbekov@mail.ru");

                string[] permissionsClaimsApprover = { "CanApproveRequest", "CanCreateRequest" };

                foreach (var claim in permissionsClaimsApprover)
                {
                    await _userManager.AddClaimAsync(approver,
                        new System.Security.Claims.Claim("Permissions", claim));
                }

                await _userManager.AddToRolesAsync(approver, new string[] { "User" });

                var performer = await _context.Users.FirstOrDefaultAsync(x => x.UserName == "inkarknnn@gmail.com");

                string[] permissionsClaimsPerformer = { "CanPerformRequest", "CanCreateRequest" };

                foreach (var claim in permissionsClaimsPerformer)
                {
                    await _userManager.AddClaimAsync(performer,
                        new System.Security.Claims.Claim("Permissions", claim));
                }

                await _userManager.AddToRolesAsync(performer, new string[] { "User" });
            }
        }

        private async Task CreateTestRequests()
        {
            var hasRequests = _context.Requests.Any();
            if (!hasRequests)
            {
                var createdStatus = await _context.RequestStatuses
                    .FirstOrDefaultAsync(x => x.Flag == Core.Abstractions.Enums.RequestStatusEnum.CREATED);

                var approvedStatus = await _context.RequestStatuses
                    .FirstOrDefaultAsync(x => x.Flag == Core.Abstractions.Enums.RequestStatusEnum.APPROVED);

                var deniedStatus = await _context.RequestStatuses
                    .FirstOrDefaultAsync(x => x.Flag == Core.Abstractions.Enums.RequestStatusEnum.DENIED);

                var adminUser = await _context.Users
                    .FirstOrDefaultAsync(x => x.UserName == "aibek.karatayev@gmail.com");

                var user = await _context.Users
                    .FirstOrDefaultAsync(x => x.UserName == "karataevolzhas@gmail.com");

                for (int i = 1; i <= 2; i++) 
                {
                    for (int j = 1; j < 17; j++) 
                    {
                        var tempRequest = new Request
                        {
                            Name = "Test request" + j,
                            Description = "Test description" + j,
                            Date = DateTime.Now,
                            Sum = new Random().Next(200, 1000),
                            RequestStatusId = createdStatus.Id,
                            CreatorId = adminUser.Id,
                            CreatedBy = "seed",
                            CreatedOn = DateTime.Now
                        };
                        if (j % 4 == 0)
                        {
                            tempRequest.RequestStatusId = approvedStatus.Id;
                        }
                        if (j % 7 == 0)
                        {
                            tempRequest.RequestStatusId = deniedStatus.Id;
                        }
                        if (i % 2 == 0) 
                        {
                            tempRequest.CreatorId = user.Id;
                        }
                        _context.Requests.Add(tempRequest);
                        _listService.Create(tempRequest.Id, "seed");
                        await _context.SaveChangesAsync();
                    }
                    
                }
                
            }
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}

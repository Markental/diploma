using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using TeamWorkAPI.Infrastructure.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using TeamWorkAPI.Data;
using TeamWorkAPI.Infrastructure.Extensions;

namespace TeamWorkAPI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public Startup(IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //_webHostEnvironment.IsDevelopment();

            services
                .AddCookieSettings()
                .AddCors()
                //.AddCors(o => o.AddPolicy("AnyCorsPolicy", builder =>
                //{
                //    builder
                //        //.SetIsOriginAllowed(isOriginAllowed: _ => true)
                //        .AllowAnyOrigin()
                //        .AllowAnyMethod()
                //        .AllowAnyHeader()
                //        .AllowCredentials();
                //}))
                .AddDatabase(_configuration)
                .AddIdentity()
                .AddJwtAuthentication(services.GetApplicationSettings(_configuration))
                .AddApplicationServices();
            //.AddSwagger();

            services.AddSwaggerGen(c =>
            {
                // Swagger 2.+ support
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey, // OAuth2
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });

                c.OrderActionsBy((apiDesc) => $"{apiDesc.ActionDescriptor.RouteValues["controller"]}_{apiDesc.HttpMethod}");

                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "TeamWorkAPI",
                    Version = "v1",
                    Description = "TeamWorkAPI",
                    TermsOfService = new Uri("https://olzhik.kz"),
                    Contact = new OpenApiContact
                    {
                        Name = "Olzhik",
                        Email = "olzhik@olzhik.kz"
                    }
                });

                // Set the comments path for the Swagger JSON and UI.                
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.SchemaFilter<EnumSchemaFilter>();
            });

            services.AddCustomAuthorization();
            services.AddCustomMvc(_webHostEnvironment.IsDevelopment());

            services.AddControllers(options =>
            {
                options.Filters.Add<ModelOrNotFoundActionFilter>();
            });

            // Worker
            services.AddHostedService<SeedWorker>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
                app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseHsts();
            //}

            app
            .UseSwaggerUI()
            .UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed((host)=>true))
            .UseHttpsRedirection()
            .UseStaticFiles()
            .UseRouting()
            .UseAuthentication()
            .UseAuthorization()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            //.ApplyMigrations();

            // Redirect to swagger by default
            //app.Run((context) =>
            //{
            //    string route = "/swagger/index.html";
            //    context.Response.Redirect(route);

            //    return Task.CompletedTask;
            //});
        }
    }
}

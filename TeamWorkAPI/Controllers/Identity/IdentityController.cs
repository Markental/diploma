﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TeamWorkAPI.Core;
using TeamWorkAPI.Core.Abstractions;
using TeamWorkAPI.Core.ApplicationSettingModels;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models;
using TeamWorkAPI.Core.Services;
using TeamWorkAPI.Infrastructure.Extensions;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using TeamWorkAPI.Core.Models.Identity;

namespace TeamWorkAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IdentityController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IIdentityService identityService;
        private readonly ApplicationSettings appSettings;

        public IdentityController(
            UserManager<ApplicationUser> userManager,
            IIdentityService identityService,
            IOptions<ApplicationSettings> appSettings)
        {
            this.userManager = userManager;
            this.identityService = identityService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Смена пароля
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(ChangePassword))]
        public async Task<ActionResult> ChangePassword(
            [FromBody] ChangePasswordRequestModel model) 
        {
            var currentUser = User.GetCurrentUser();

            var user = await userManager.FindByIdAsync(currentUser.Id);
            if (user == null) 
            {
                return NotFound();
            }

            var result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

            if(result.Succeeded) 
            {
                // sendgrid
                var apiKey = appSettings.SendGridApiKey;
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress("karataevolzhas@gmail.com", "TeamWork");
                var subject = "Пароль вашей учетной записи TeamWork изменен";
                var to = new EmailAddress(user.Email, user.FullName());
                var plainTextContent = 
                    $"Здравствуйте, <var>{user.FullName()}</var>! Пароль к вашей учетной записи TeamWork недавно был изменен. " +
                    $"Если его изменили вы, то вам ничего не нужно предпринимать. " +
                    $"Если это были не вы, возможно кто-то получил доступ к вашей учетной записи. Пока что сброс пароля не реализован, но вы держитесь!";
                var htmlContent = 
                    $"<h2>Здравствуйте, {user.FullName()}!</h2>" +
                    $"<p>Пароль к вашей учетной записи TeamWork недавно был изменен.</p>" +
                    $"<p>Если его изменили вы, то вам ничего не нужно предпринимать.</p>" +
                    $"<p>Если это были не вы, возможно, кто-то получил доступ к вашей учетной записи. Пока что сброс пароля не реализован, но вы держитесь!</p>";
                var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                var response = await client.SendEmailAsync(msg);

                //
                return Ok();
            }

            return BadRequest(result.Errors);
        }



        /// <summary>
        /// Логин
        /// "karataevolzhas@gmail.com"
        /// "Qwerty123!"
        /// </summary>
        /// <param name="model"></param>
        /// <returns>токен с</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route(nameof(Login))]
        public async Task<ActionResult> Login([FromBody]LoginRequestModel model)
        {
            var user = await userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                return Unauthorized();
            }

            var passwordValid = await userManager.CheckPasswordAsync(user, model.Password);
            if (!passwordValid)
            {
                return Unauthorized();
            }

            var claims = new List<Claim>();
            claims.Add(new Claim("FirstName", user.FirstName));
            claims.Add(new Claim("LastName", user.LastName));
            claims.Add(new Claim("Position", user.Position));

            var userClaims = await userManager.GetClaimsAsync(user);
            var permissionClaim = userClaims.Where(x => x.Type == "Permissions");

            if (permissionClaim != null)
            {
                claims.AddRange(permissionClaim);
            }

            // теперь claims хранятся отдельно без делиметра |

            //if (!string.IsNullOrWhiteSpace(permissionClaim))
            //{
            //    claims.AddRange(permissionClaim.Split('|')
            //        .Select(item => new Claim(item, item)));
            //}
            
            var role = (await userManager.GetRolesAsync(user)).FirstOrDefault();
            var token = identityService.GenerateJwtToken(userId: user.Id,
                userName: user.UserName,
                role: role,
                secret: appSettings.Secret,
                customClaims: claims);

            return Ok(new { token, user.UserName, FullName = user.FullName(), user.Id, claims });
        }

    }
}

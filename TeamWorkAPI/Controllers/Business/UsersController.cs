﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Identity;
using TeamWorkAPI.Infrastructure.Extensions;

namespace TeamWorkAPI.Controllers.Business
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;

        public UsersController(IWebHostEnvironment appEnvironment,
           ApplicationDbContext context,
           UserManager<ApplicationUser> userManager,
           RoleManager<ApplicationRole> roleManager)
        {
            _appEnvironment = appEnvironment;
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // Claims - CanApproveRequest, CanPerformRequest, CanCreateRequest, CanEditUsers

        /// <summary>
        /// Получить коллекцию с аппруверами для оборота заявки
        /// </summary>
        /// <returns>Collection of users elligible for approving request</returns>
        [HttpGet]
        [Route(nameof(GetApprovers))]
        public async Task<ActionResult<IList<ApplicationUser>>> GetApprovers()
        {
            var currentApplicationUser = await _userManager.GetUserAsync(User);
            var approverClaim = new Claim("Permissions", "CanApproveRequest");
            var approvers = await _userManager.GetUsersForClaimAsync(approverClaim);

            approvers.Remove(currentApplicationUser);

            return Ok(approvers); // вернуть другую модель!
        }

        /// <summary>
        /// Получить коллекцию с перформерами для оборота заявки
        /// </summary>
        /// <returns>Collection of users elligible for approving request</returns>
        [HttpGet]
        [Route(nameof(GetPerformers))]
        public async Task<ActionResult<IList<ApplicationUser>>> GetPerformers()
        {
            var approverClaim = new Claim("Permissions", "CanPerformRequest");
            var approvers = await _userManager.GetUsersForClaimAsync(approverClaim);

            return Ok(approvers);  // вернуть другую модель!
        }

        [HttpGet]
        [Route(nameof(GetUserFullNameById) + "/{userId}")] // localhost:5001/api/Users/GetUserFullNameById/IdHere
        public async Task<ActionResult<IList<ApplicationUser>>> GetUserFullNameById([FromRoute] string userId)
        {
            var foundUser = await _userManager.FindByIdAsync(userId);

            if (foundUser == null) 
            {
                return NotFound();
            }

            return Ok(new { UserName = foundUser.FullName() });
        }
    }
}

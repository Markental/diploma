﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TeamWorkAPI.Core.Abstractions;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models;

namespace TeamWorkAPI.Controllers.Business
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public AdminController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            ApplicationDbContext context)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpGet]
        [Route(nameof(GetUsersList))]
        public async Task<(IList<ApplicationUser> Collection, int TotalCount)> GetUsersList(
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            if (skip == null)
            {
                skip = -1;
            }

            // Max elements on page
            if (top == null)
            {
                top = -1;
            }

            var users = _context.Users.AsQueryable();

            users = users.OrderByDescending(x => x.UserName);
            var totalCount = users.Count();

            if (!(skip == -1))
            {
                users = users.Skip(skip.Value * top.Value);
            }

            if (!(top == -1))
            {
                users = users.Take(top.Value);
            }

            var result = await users
                .AsNoTracking()
                .ToListAsync();

            foreach (ApplicationUser user in users)
            {
                user.PasswordHash = null;
            }

            return (result, totalCount);
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpGet]
        [Route(nameof(GetPermissions))]
        public async Task<IEnumerable<Claim>> GetPermissions() 
        {
            var result = await Task.Run(getClaimsList);

            return result;
        }

        // Claims - CanApproveRequest, CanPerformRequest, CanCreateRequest, CanEditUsers
        [ApiExplorerSettings(IgnoreApi = true)]
        public IEnumerable<Claim> getClaimsList() 
        {
            string[] claims = { "CanApproveRequest", "CanPerformRequest", "CanCreateRequest", "CanEditUsers" };
            List<Claim> claimsList = new List<Claim>();
            foreach (string claim in claims)
            {
                claimsList.Add(new Claim("Permissions", claim));
            }
            return claimsList;
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpGet]
        [Route(nameof(GetRoles))]
        public async Task<IEnumerable<ApplicationRole>> GetRoles()
        {
            var roles = await _roleManager.Roles.ToListAsync();

            return roles;
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpGet]
        [Route(nameof(GetOneUser) + "/{userId}")]
        public async Task<ActionResult<(ApplicationUser User, IList<ApplicationRole> Roles, IList<Claim> Claims)>> GetOneUser([FromRoute] string userId) 
        {
            var user = await _context.Users.Where(x => x.Id == userId).AsNoTracking().FirstOrDefaultAsync();
            var roles = await _userManager.GetRolesAsync(user);
            var claims = await _userManager.GetClaimsAsync(user);

            return Ok((user, roles, claims));
        }

        /// <summary>
        /// Админское создание пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpPost]
        [Route(nameof(CreateUser))]
        public async Task<ActionResult> CreateUser([FromBody] RegisterRequestModel model)
        {
            var user = new ApplicationUser
            {
                Email = model.Username,
                UserName = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Position = model.Position,
                PhoneNumber = model.PhoneNumber
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "User");
                var createdUser = await _userManager.FindByNameAsync(user.UserName);
                return Ok(createdUser.Id);
            }

            return BadRequest(result.Errors);
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpPost]
        [Route(nameof(AddRole) + "/{userId}")]
        public async Task<ActionResult> AddRole(
            [FromRoute] string userId,
            [FromQuery] string role) 
        {
            if (!(string.IsNullOrWhiteSpace(userId) && !(string.IsNullOrWhiteSpace(role)))) 
            {
                var user = await _userManager.FindByIdAsync(userId);
                var foundRole = await _roleManager.FindByNameAsync(role);
                if (user != null && foundRole != null) 
                {
                    await _userManager.AddToRoleAsync(user, foundRole.Name);
                    return Ok();
                }
            }

            return BadRequest();
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpPost]
        [Route(nameof(RemoveRole) + "/{userId}")]
        public async Task<ActionResult> RemoveRole(
            [FromRoute] string userId,
            [FromQuery] string role)
        {
            if (!(string.IsNullOrWhiteSpace(userId) && !(string.IsNullOrWhiteSpace(role))))
            {
                var user = await _userManager.FindByIdAsync(userId);
                var foundRole = await _roleManager.FindByNameAsync(role);
                if (user != null && foundRole != null)
                {
                    await _userManager.RemoveFromRoleAsync(user, foundRole.Name);
                    return Ok();
                }
            }

            return BadRequest();
        }

        // Claims - CanApproveRequest, CanPerformRequest, CanCreateRequest, CanEditUsers
        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpPost]
        [Route(nameof(AddPermission) + "/{userId}")]
        public async Task<ActionResult> AddPermission(
            [FromRoute] string userId,
            [FromQuery] string permission)
        {
            if (!(string.IsNullOrWhiteSpace(userId) && !(string.IsNullOrWhiteSpace(permission)))) 
            {
                var user = await _userManager.FindByIdAsync(userId);
                var claim = new Claim("Permissions", permission);

                await _userManager.AddClaimAsync(user, claim);
                return Ok();
            }

            return BadRequest();
        }

        [Authorize(Policy = Policies.CanEditUsersPolicy, Roles = "Admin")]
        [HttpPost]
        [Route(nameof(RemovePermission) + "/{userId}")]
        public async Task<ActionResult> RemovePermission(
            [FromRoute] string userId,
            [FromQuery] string permission)
        {
            if (!(string.IsNullOrWhiteSpace(userId) && !(string.IsNullOrWhiteSpace(permission))))
            {
                var user = await _userManager.FindByIdAsync(userId);
                var claim = new Claim("Permissions", permission);

                await _userManager.RemoveClaimAsync(user, claim);
                return Ok();
            }

            return BadRequest();
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using TeamWorkAPI.Core.Abstractions;
using TeamWorkAPI.Core.ApplicationSettingModels;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Models.Request;
using TeamWorkAPI.Core.Services;
using TeamWorkAPI.Infrastructure.Extensions;

namespace TeamWorkAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestsController : Controller
    {
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly ApplicationDbContext _context;
        private readonly IRequestsService _requestsService;
        private readonly IRequestFilesService _requestFilesService;
        private readonly IApprovalListService _approvalListService;
        private readonly ApplicationSettings appSettings;

        public RequestsController(IWebHostEnvironment appEnvironment,
            ApplicationDbContext context,
            IRequestsService requestsService,
            IRequestFilesService requestFilesService,
            IApprovalListService approvalListService,
            IOptions<ApplicationSettings> appSettings)
        {
            _appEnvironment = appEnvironment;
            _context = context;
            _requestsService = requestsService;
            _requestFilesService = requestFilesService;
            _approvalListService = approvalListService;
            this.appSettings = appSettings.Value;
        }

        [HttpGet]
        [Route(nameof(GetRecent))]
        public async Task<Request> GetRecent()
        {
            var user = User.GetCurrentUser();

            var request = await _requestsService.RecentRequest(user);

            return request;
        }

        [HttpGet]
        [Route(nameof(GetPendingCount))]
        public async Task<int> GetPendingCount()
        {
            var user = User.GetCurrentUser();

            return await _requestsService.PendingRequestsCount(user);
        }

        [HttpGet]
        [Route(nameof(GetApprovedCount))]
        public async Task<int> GetApprovedCount()
        {
            var user = User.GetCurrentUser();

            return await _requestsService.ApprovedRequestsCount(user);
        }

        [HttpGet]
        [Route(nameof(GetRequestById) + "/{RequestId}")]
        public async Task<Request> GetRequestById([FromRoute] Guid RequestId)
        {
            var request = await _requestsService.GetOneById(RequestId);

            return request;
        }

        [HttpGet]
        [Route(nameof(GetStatuses))]
        public async Task<IEnumerable<RequestStatus>> GetStatuses()
        {
            return await _requestsService.GetStatuses();
        }

        [Authorize(Policy = Policies.CanApproveRequestPolicy)]
        [HttpGet]
        [Route(nameof(GetRequestsToApprove))]
        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetRequestsToApprove(
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            var user = User.GetCurrentUser();

            if (skip == null)
            {
                skip = -1;
            }

            // Max elements on page
            if (top == null)
            {
                top = -1;
            }

            var requests = await _requestsService.GetRequestsForCurrentUsersApproval(user, skip.Value, top.Value);

            return requests;
        }

        [Authorize(Policy = Policies.CanPerformRequestPolicy)]
        [HttpGet]
        [Route(nameof(GetAllApprovedRequests))]
        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetAllApprovedRequests(
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            var user = User.GetCurrentUser();

            if (skip == null)
            {
                skip = -1;
            }

            // Max elements on page
            if (top == null)
            {
                top = -1;
            }

            var requests = await _requestsService.GetApprovedRequests(skip.Value, top.Value);

            return requests;
        }

        [Authorize(Policy = Policies.CanPerformRequestPolicy)]
        [HttpGet]
        [Route(nameof(GetAllDoneRequests))]
        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetAllDoneRequests(
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            var user = User.GetCurrentUser();

            if (skip == null)
            {
                skip = -1;
            }

            // Max elements on page
            if (top == null)
            {
                top = -1;
            }

            var requests = await _requestsService.GetCompletedRequests(skip.Value, top.Value);

            return requests;
        }

        /// <summary>
        /// Список созданных текущим юзером заявок
        /// </summary>
        /// <param name="name"></param>
        /// <param name="statusId"></param>
        /// <param name="sum"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="skip"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(nameof(GetFilteredList))]
        public async Task<(IEnumerable<Request> Collection, int TotalCount)> GetFilteredList([FromQuery] string name,
            [FromQuery] Guid? statusId,
            [FromQuery] double? sum,
            [FromQuery] DateTime? dateFrom,
            [FromQuery] DateTime? dateTo,
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            var user = User.GetCurrentUser();

            // Page
            if (skip == null)
            {
                skip = -1;
            }

            // Max elements on page
            if (top == null)
            {
                top = -1;
            }

            var result = await _requestsService.GetFilteredList(user.Id,
                name,
                statusId,
                sum,
                dateFrom,
                dateTo,
                skip.Value,
                top.Value);

            return result;
        }

        //[Authorize(Policy = Policies.CanCreateRequestPolicy)]
        //[HttpPost]
        //[Route(nameof(Create))]
        //public async Task<ActionResult<int>> Create([FromForm]string name)
        //{
        //    var user = User.GetCurrentUser();

        //    var requestId = await _requestsService.Create(user, name);

        //    return CreatedAtAction(nameof(this.Create), new { RequestId = requestId });
        //}

        /// <summary>
        /// Создать Заявку (одним post запросом); Date записывать в таком формате: 27.04.2021 23:09:05
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpPost]
        [Route(nameof(CreateWhole))]
        public async Task<ActionResult<int>> CreateWhole([FromForm] RequestVM model) // Tested
        {
            var user = User.GetCurrentUser();

            if (model.Files != null)
            {
                foreach (var file in model.Files)
                {
                    // Check extension
                    var extension = System.IO.Path.GetExtension(file.FileName);
                    if (extension != ".pdf")
                    {
                        return BadRequest("Один из файлов не является pdf");
                    }
                    // 10 MB Limit
                    if (file.Length > 1024 * 1024 * 2)
                    {
                        return BadRequest("Файл больше 2 МБ");
                    }
                }
            }
            var requestId = await _requestsService.CreateWhole(
                model.Name,
                model.Description,
                model.Sum,
                model.Date,
                model.Files,
                user);

            return CreatedAtAction(nameof(this.CreateWhole), new { RequestId = requestId });
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpPut]
        [Route(nameof(Edit))]
        public async Task<ActionResult> Edit( 
            [FromQuery] Guid requestId,
            [FromQuery] string name,
            [FromQuery] string description,
            [FromQuery] double? sum,
            [FromQuery] DateTime? date)
        {
            var user = User.GetCurrentUser();

            var changed = await _requestsService.Edit(requestId, name, description, sum.Value, date.Value, user);

            if (!changed)
            {
                return BadRequest();
            }

            return Ok();
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpPost]
        [Route(nameof(AddNewFilesToRequest) + "/{requestId}")]
        public async Task<ActionResult> AddNewFilesToRequest([FromForm] IList<IFormFile> uploads, [FromRoute] Guid requestId)
        {
            var user = User.GetCurrentUser();

            var request = await _requestsService.GetOneById(requestId);

            if (user.Id != request.CreatorId)
            {
                return BadRequest("У Вас недостаточно прав для редактирования этой заявки");
            }

            foreach (var file in uploads)
            {
                // Check extension
                var extension = Path.GetExtension(file.FileName);
                if (extension != ".pdf")
                {
                    return BadRequest("Один из файлов не является pdf");
                }
                // 10 MB Limit
                if (file.Length > 1024 * 1024 * 2)
                {
                    return BadRequest("Файл больше 2 МБ");
                }
            }

            await _requestFilesService.AddFilesToMongo(uploads, requestId, user.FullName());

            return Ok();
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpGet]
        [Route(nameof(GetRequestFiles) + "/{requestId}")]
        public async Task<ActionResult<IEnumerable<RequestFile>>> GetRequestFiles([FromRoute] Guid requestId)
        {
            var user = User.GetCurrentUser();

            var requestFiles = await _requestFilesService.GetRequestFiles(requestId);

            return Ok(requestFiles);
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpDelete]
        [Route(nameof(DeleteFileFromRequest) + "/{fileId}")]
        public async Task<ActionResult> DeleteFileFromRequest([FromRoute] Guid fileId) 
        {
            var user = User.GetCurrentUser();

            var file = await _requestFilesService.GetRequestFile(fileId);

            if (user.Id != file.Request.CreatorId)
            {
                return BadRequest("У Вас недостаточно прав для редактирования этой заявки");
            }

            await _requestFilesService.MongoDeleteFile(fileId);

            return Ok();
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpDelete]
        [Route(nameof(DeleteRequest) + "/{requestId}")]
        public async Task<ActionResult> DeleteRequest([FromRoute] Guid requestId) 
        {
            var user = User.GetCurrentUser();

            var success = await _requestsService.Delete(requestId, user);

            if (success) 
            {
                return Ok();
            }

            return BadRequest();
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpPost]
        [Route(nameof(SendForApproval))]
        public async Task<ActionResult> SendForApproval([FromBody] SendForApprovalRequestModel model)
        {
            var user = User.GetCurrentUser();

            var success = await _requestsService.SendForApproval(model.RequestId, user);

            if (success)
            {
                var request = await _requestsService.GetOneById(model.RequestId);
                var apiKey = appSettings.SendGridApiKey;
                var client = new SendGridClient(apiKey);
                foreach (ApprovalListMember member in request.ApprovalList.Members) 
                {
                    // sendgrid
                    var from = new EmailAddress("karataevolzhas@gmail.com", "TeamWork");
                    var subject = "Вы были назначены на подтверждение заявки";
                    var to = new EmailAddress(member.Member.Email, user.FullName());
                    var plainTextContent =
                        $"Вы были назначены на подтверждение заявки, созданной пользователем {request.Creator.FullName()}, который занимает должность {request.Creator.Position}. " +
                        $"Крайняя дата заявки - {request.Date.Date}.";
                    var htmlContent =
                        $"<p>Вы были назначены на подтверждение заявки <var>{request.Name}</var>, созданной пользователем <var>{request.Creator.FullName()}</var>, который занимает должность <var>{request.Creator.Position}.</var></p>" +
                        $"<p>Крайняя дата заявки - <strong>{request.Date.Date}</strong></p>"; // мб добавить AddDays(-1) к дате
                    var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
                    var response = await client.SendEmailAsync(msg);
                    //
                }


                return Ok();
            }

            return BadRequest();
        }

        [Authorize(Policy = Policies.CanCreateRequestPolicy)]
        [HttpPost]
        [Route(nameof(RequestCompleted))]
        public async Task<ActionResult> RequestCompleted([FromBody] SendForApprovalRequestModel model)
        {
            var user = User.GetCurrentUser();

            var success = await _requestsService.CompleteRequest(model.RequestId, user);

            if (success)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route(nameof(GetFile) + "/{fileId}" + "/{fileName}")]
        public async Task<ActionResult> GetFile([FromRoute] Guid fileId)
        {
            var requestFile = await _requestFilesService.GetMongoRequestFile(fileId);

            if (requestFile.Metadata == null || requestFile.Content == null)
            {
                return NotFound();
            }

            Response.Headers.Add("Content-Disposition", $"inline; filename={requestFile.Metadata.Name}");
            return File(requestFile.Content, requestFile.Metadata.MimeType);
        }

        // public async Task<IActionResult> DeleteFile([FromRoute] int fileId)

        //    /// <summary>
        //    /// ОТДЕЛЬНЫЙ АПЛОАД
        //    /// </summary>
        //    /// <param name="requestId"></param>
        //    /// <param name="file"></param>
        //    /// <returns></returns>
        //    [HttpPost]
        //    [Route(nameof(UploadFiles) + "/{requestId}")]
        //    public async Task<IActionResult> UploadFiles([FromRoute] int requestId,
        //        [FromForm] IFormFile file)
        //    {
        //        // Check extension
        //        var extension = System.IO.Path.GetExtension(file.FileName);
        //        if (extension != ".pdf")
        //        {
        //            return BadRequest("Файл не является pdf");
        //        }

        //        // 10 MB Limit
        //        if (file.Length > 1024 * 1024 * 10)
        //        {
        //            return BadRequest("Файл больше 10 МБ");
        //        }

        //        //foreach (var upload in uploads)
        //        //{
        //        //    string path = "/Files/" + upload.FileName;
        //        //    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
        //        //    {
        //        //        await upload.CopyToAsync(fileStream);
        //        //    }
        //        //    var file = new RequestFile
        //        //    {
        //        //        Path = path,
        //        //        Name = upload.FileName + "_" + DateTime.Now.Ticks,
        //        //        FileType = upload.ContentType,
        //        //        CreatedOn = DateTime.Now,
        //        //    };
        //        //    _context.Files.Add(file);
        //        //}
        //        //_context.SaveChanges();

        //        return Ok();
        //    }

        //    /// <summary>
        //    /// ЕДИНЫЙ АПЛОАД С МЕТАДАННЫМИ
        //    /// </summary>
        //    /// <param name="requestId"></param>
        //    /// <param name="file"></param>
        //    /// <returns></returns>
        //    [HttpPost]
        //    [Route(nameof(UploadFiles2))]
        //    public async Task<IActionResult> UploadFiles2(
        //        [FromForm] RequestVM model)
        //    {
        //        //foreach (var upload in uploads)
        //        //{
        //        //    string path = "/Files/" + upload.FileName;
        //        //    using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
        //        //    {
        //        //        await upload.CopyToAsync(fileStream);
        //        //    }
        //        //    var file = new RequestFile
        //        //    {
        //        //        Path = path,
        //        //        Name = upload.FileName + "_" + DateTime.Now.Ticks,
        //        //        FileType = upload.ContentType,
        //        //        CreatedOn = DateTime.Now,
        //        //    };
        //        //    _context.Files.Add(file);
        //        //}
        //        //_context.SaveChanges();

        //        return Ok();
        //    }

        //    [HttpGet]
        //    [Route(nameof(DownloadFile) + "/{fileId}")]
        //    public async Task<IActionResult> DownloadFile([FromRoute] int fileId)
        //    {
        //        var fileMeta = await _context.RequestFiles.FirstOrDefaultAsync(x => x.Id == fileId);

        //        // fileMeta.StorageId
        //        byte[] content = null;

        //        // application/pdf
        //        return File(content, fileMeta.MimeType, fileMeta.Name);
        //    }
        //}

    }
}

﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TeamWorkAPI.Core.ContextEntities;
using TeamWorkAPI.Core.Services;
using TeamWorkAPI.Infrastructure.Extensions;
using TeamWorkAPI.Core.Models;
using Microsoft.AspNetCore.Authorization;
using TeamWorkAPI.Core.Abstractions;
using TeamWorkAPI.Core.Models.Request;

namespace TeamWorkAPI.Controllers.Business
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApprovalListController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IApprovalListService _approvalListService;
        private readonly UserManager<ApplicationUser> _userManager;

        public ApprovalListController(IWebHostEnvironment appEnvironment,
            ApplicationDbContext context,
            IApprovalListService approvalListService,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _approvalListService = approvalListService;
            _userManager = userManager;
        }

        [HttpGet]
        [Route(nameof(GetByRequestId) + "/{requestId}")]
        public async Task<ActionResult<ApprovalList>> GetByRequestId([FromRoute] Guid requestId)
        {
            var approvalList = await _approvalListService.GetOneByListId(requestId);

            // NotFound done by action filter
            return Ok(approvalList);
        }

        //[HttpPost]
        //[Route(nameof(Create))]
        //public async Task<IActionResult> Create(Guid requestId)
        //{
        //    var user = User.GetCurrentUser();

        //    var id = await _approvalListService.Create(requestId, user.FullName());

        //    return CreatedAtAction(nameof(Create), new { ListId = id });
        //}

        [HttpPost]
        [Route(nameof(AddMember))]
        public async Task<ActionResult> AddMember([FromForm] AddMemberRequestModel model)
        {
            var currentUser = User.GetCurrentUser();

            var member = await _userManager.FindByIdAsync(model.UserId);

            if (member == null) 
            {
                return NotFound();
            }

            var id = await _approvalListService.AddApproverToList(member, model.ListId, currentUser.FullName());

            return CreatedAtAction(nameof(AddMember), id);
        }

        [HttpPost]
        [Route(nameof(RemoveMember))]
        public async Task<ActionResult> RemoveMember([FromBody] RemoveMemberRequestModel model)
        {
            await _approvalListService.RemoveApproverFromList(model.MemberId);

            return Ok();
        }

        [Authorize(Policy = Policies.CanApproveRequestPolicy)]
        [HttpPost]
        [Route(nameof(ApproveStep))]
        public async Task<ActionResult> ApproveStep([FromBody] ApproveStepRequestModel model) 
        {
            var currentUser = User.GetCurrentUser();

            var success = await _approvalListService.ApproveStep(model.RequestId, currentUser);

            if (success)
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpPost]
        [Route(nameof(DeclineStep))]
        public async Task<ActionResult> DeclineStep([FromBody] DeclineStepRequestModel model)
        {
            var currentUser = User.GetCurrentUser();

            var success = await _approvalListService.DeclineStep(model.RequestId, model.CancelMessage, currentUser);

            if (success)
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}
